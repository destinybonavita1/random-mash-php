<?php
$title = "Welcome to Random Mash!";
include("Templates/Head.php");
?>
<div id="body">
    <form action="Login.php" method="POST">
        <table style='width: 40%; background-color: #FAFAFA; border: 4px solid #E8E8E6; padding: 5px; margin: auto'>
            <tr>
                <td colspan='2' style='text-align: center; border-bottom: 1px solid'>
                    Login
                </td>
            </tr>
            <tr>
                <td align="right" style="width: 50%">
                    Username:
                </td>
                <td>
                    <input type="text" name="username" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    Password:
                </td>
                <td>
                    <input type="password" name="password" />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding: 10px">

                </td>
            </tr>
            <tr>
                <td align="center">
                    <?php
                    if (isset($_POST["username"]) && isset($_POST["password"])) {
                        if (strlen($_POST["username"]) > 0 && strlen($_POST["password"]) > 0)
                            $api2->Login($_POST["username"], $_POST["password"]);
                    } else {
                        echo "Please login.";
                    }
                    ?>
                </td>
                <td align="center">
                    <input type="submit" value="Login" />
                    <input type="button" onClick="window.location = 'Register.php'" value="Register" />
                    <?php $fb->GetSignin(); ?>
                </td>
            </tr>
        </table>
    </form>
</div>
<?php
if ($fb->CheckCallback() && !$api2->IsSignedIn()) {
    if (!$fb->CheckExists() && $_GET["d"] != "exists") {
        echo '<script>'
        . 'var data = createFBUsername("' . $fb->facebook->getUser() . '");'
        . 'console.log(data);'
        . '</script>';
    } else {
        $fb->Login($fb->facebook->getUser());
    }
}