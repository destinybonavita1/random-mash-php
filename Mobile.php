<?php
$title = "Welcome to Random Mash!";
include("Templates/Head.php");
?>
<style>
    html
    {
        height: 100%;
        max-width: 280px;
    }
</style>
<meta name="viewport" content="width=280"/>
<link rel="stylesheet" href="/Styles/Site.css?14<?php echo rand() ?>" />
 <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-54095167-1', 'auto');
        ga('send', 'pageview');

    </script>
<h2 style="text-align: center">
    Welcome to Random Mash!<br/>
    <h4 style="text-align: center">Download our mobile app or go to the website on a computer.</h4>
</h2>
<div class="footer" style="vertical-align: middle">
    <a style="width: 50%" href="https://itunes.apple.com/us/app/random-mash/id843497271?mt=8"><img src="/Images/App_Store.jpg"/></a>
    <a style="width: 50%" href="https://play.google.com/store/apps/details?id=com.nms.randommash&hl=en"><img src="/Images/play_store.png" style="width: 50%"/></a>
</div>