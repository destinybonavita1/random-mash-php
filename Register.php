<?php
$title = "Welcome to Random Mash!";
include("Templates/Head.php");
?>
<div id="body">
    <form action="Register.php" method="POST">
        <table style='width: 40%; background-color: #FAFAFA; border: 4px solid #E8E8E6; padding: 5px; margin: auto'>
            <tr>
                <td colspan='2' style='text-align: center; border-bottom: 1px solid'>
                    Register
                </td>
            </tr>
            <tr>
                <td align="right" style="width: 50%">
                    Username:
                </td>
                <td>
                    <input type="text" name="username" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    Password:
                </td>
                <td>
                    <input type="password" name="password" />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding: 10px">

                </td>
            </tr>
            <tr>
                <td align="center">
                    <?php
                    if (isset($_POST["username"]) && isset($_POST["password"])) {
                        $api2->Register($_POST["username"], $_POST["password"]);
                    } else {
                        echo "Please register.";
                    }
                    ?>
                </td>
                <td align="center">
                    <input type="submit" value="Register" />
                    <input type="button" onClick="window.location = 'login.php'" value="Login" />
                </td>
            </tr>
        </table>
    </form>
</div>