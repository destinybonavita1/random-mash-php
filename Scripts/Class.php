<?php

$api2 = new DataAPIM();
$notifAPI = new NotificationAPI();
if (isset($_GET["a"])) {
    if ($_GET["a"] == "logout")
        $api2->Logout();
}

class DataAPIM {

    private $db;
    private $notif;
    public $postId;

    function __construct() {
        $this->db = new mysqli('localhost', 'randomma', "i'llbeokay14", 'randomma_rm', 8889);
        $this->db->autocommit(TRUE);
        $this->notif = new NotificationAPI();
    }

    function __destruct() {
        $this->db->close();
        session_commit();
    }

    #standard functions

    function categoriesNew() {
        $cats = array("All", "Anti Jokes", "Bad Insults", "Burns", "Cringe Worthy", "Conversations", "Conversation Starters", "Dirty Minded", "Emoji Stories", "Feels", "#ICant", "Icons", "Punny Jokes", "Pickup Lines", "Random", "Rhetorical questions", "Unneeded Hashtags", "Yo Momma");

        foreach ($cats as $str) {
            echo "<option>" . $str . "</option>";
        }
    }

    function categoriesArray() {
        $cats = array("Anti Jokes", "Bad Insults", "Burns", "Cringe Worthy", "Conversations", "Conversation Starters", "Dirty Minded", "Emoji Stories", "Feels", "#ICant", "Icons", "Punny Jokes", "Pickup Lines", "Random", "Rhetorical questions", "Unneeded Hashtags", "Yo Momma");
        $cat = "";

        foreach ($cats as $str) {
            $cat = $cat . "<option>" . $str . "</option>";
        }

        return $cat;
    }

    function IsSignedIn() {
        if (session_status() == PHP_SESSION_ACTIVE) {
            if (isset($_SESSION["UID"])) {
                return TRUE;
            }

            return FALSE;
        }

        return FALSE;
    }

    function returnedSQLCall($query, $function, $types = "", $params = array(), $results = array()) {
        $stmt = $this->db->prepare($query);

//        $parVal = "";
//        foreach ($params as $par) {
//            $parVal .= $par;
//        }
        error_log($query);
        error_log($types);
        error_log(json_encode($params));
        call_user_func(array($stmt, "bind_param"), array_merge(array($types), array($params)));

        $stmt->execute();
        foreach ($results as $result) {
            $stmt->bind_result($result);
        }

        while ($stmt->fetch()) {
            $function();
        }

        $stmt->close();
    }

    #User handling

    function Login($user, $pass) {
        $stmt = $this->db->prepare("SELECT COUNT(*) FROM Users WHERE Alias = ?");
        $stmt->bind_param("s", $user);
        $stmt->execute();
        $stmt->bind_result($count);
        while ($stmt->fetch()) {
            break;
        }
        $stmt->close();

        if ($count == 0)
            echo '<a class="error" style="font-size: small">That account does not exist.</a>';
        else {
            $stmt = $this->db->prepare("SELECT ID FROM Users WHERE Alias = ? AND Password = ?");
            $stmt->bind_param("ss", $user, $pass);
            $stmt->execute();
            $stmt->bind_result($UID);
            while ($stmt->fetch()) {
                break;
            }
            $stmt->close();

            if ($UID != NULL) {
                $_SESSION["UID"] = $UID;
                echo '<script type="text/javascript"> window.location.href = "index.php" </script>';
            } else
                echo '<a class="error" style="font-size: small">Your username or password is incorrect.</a>';
        }
    }

    function Register($user, $pass, $autologin = true) {
        $stmt = $this->db->prepare("SELECT COUNT(*) FROM Users WHERE Alias = ?");
        $stmt->bind_param("s", $user);
        $stmt->execute();
        $stmt->bind_result($count);
        while ($stmt->fetch()) {
            break;
        }
        $stmt->close();

        if ($count > 0)
            echo '<script type="text/javascript">alert("That username is already taken.");</script>';
        else {
            $stmt = $this->db->prepare("INSERT INTO Users (Alias, Password) VALUES (?, ?)");
            $stmt->bind_param("ss", $user, $pass);
            $stmt->execute();
            $stmt->close();

            $stmt = $this->db->prepare("SELECT ID FROM Users WHERE Alias = ?");
            $stmt->bind_param("s", $user);
            $stmt->execute();
            $stmt->bind_result($UID);
            while ($stmt->fetch()) {
                break;
            }
            $stmt->close();

            if ($autologin) {
                $_SESSION["UID"] = $UID;
                echo '<script type="text/javascript"> window.location.href = "index.php" </script>';
            } else {
                return $UID;
            }
        }
    }

    function Logout() {
        session_destroy();
    }

    function getAlias($userId, $return = false) {
        $stmt = $this->db->prepare("SELECT Alias FROM Users WHERE ID = ?");
        $stmt->bind_param("i", $userId);
        $stmt->execute();
        $stmt->bind_result($alias);
        while ($stmt->fetch()) {
            break;
        }

        $stmt->close();

        if (!$return)
            echo $alias;
        else
            return $alias;
    }

    function searchUsers($user) {
        if (isset($user)) {
            $this->searchAlias($user);
        } else {
            echo "<table style='width: 100%; background-color: #FAFAFA; border: 4px solid #E8E8E6; padding: 5px'>
                                                <tr>
                                                        <td style='text-align: center;'>
                                                        Please search for a username. 
                                                        </td>
                                                </tr>
                                        </table>";
        }
    }
    
    function searchAlias($alias) {
        $snd = "%" . $alias . "%";
        $resultCount = 0;
        $stmt = $this->db->prepare("SELECT Alias, ID, (SELECT COUNT(*) FROM Posts WHERE UserID = Users.ID) AS Count FROM Users WHERE Alias LIKE ?");
        $stmt->bind_param("s", $snd);
        $stmt->execute();
        $stmt->bind_result($a, $id, $postCount);
        while ($stmt->fetch()) {

            $resultCount++;
            echo "
				<table style='width: 100%; background-color: #FAFAFA; border: 4px solid #E8E8E6; padding: 5px'>
					<tr>
						<td style='text-align: left;'>
							<a href='User.php?i=" . $id . "'>" . $a . "</a>
						</td>
						<td style='text-align: right'>
							Posted " . $postCount . " times.
						</td>
					</tr>
				</table>
				<br />";
        }

        if ($resultCount == 0) {
            echo "
				<table style='width: 100%; background-color: #FAFAFA; border: 4px solid #E8E8E6; padding: 5px'>
					<tr>
						<td style='text-align: center;'>
							No account named " . '"' . $alias . '"' . " exists.
						</td>
					</tr>
				</table>
				<br />";
        }

        $stmt->close();
    }

    function followUnfollow($follower, $following) {
        if ($this->isFollowing($follower, $following)) {
            $stmt = $this->db->prepare("DELETE FROM Following WHERE Follower = ? AND Following = ?");
            $stmt->bind_param("ii", $follower, $following);
            $stmt->execute();
            $stmt->close();
        } else {
            $stmt = $this->db->prepare("INSERT INTO Following (Follower, Following) VALUES (?, ?)");
            $stmt->bind_param("ii", $follower, $following);
            $stmt->execute();
            $stmt->close();

            $stmt = $this->db->prepare("SELECT Alias FROM Users WHERE ID = ?");
            $stmt->bind_param("i", $follower);
            $stmt->execute();
            $stmt->bind_result($alias);

            while ($stmt->fetch()) {
                break;
            }

            $stmt->close();

            $this->notif->sendNotification($following, $alias . " is now following you.", $follower, NULL);
            $this->notif->SendNotifications(array($following), $alias . " is now following you.", array("UserId" => $follower));
        }
    }

    function isFollowing($follower, $following) {
        $stmt = $this->db->prepare("SELECT COUNT(*) FROM Following WHERE Follower = ? AND Following = ?");
        $stmt->bind_param("ii", $follower, $following);
        $stmt->execute();
        $stmt->bind_result($count);
        while ($stmt->fetch()) {
            break;
        }
        $stmt->close();
        return ($count > 0) ? true : false;
    }

    #Settings handling

    function getSetting($setting) {
        if ($this->IsSignedIn()) {
            $uid = $_SESSION["UID"];

            switch ($setting) {
                case "RandomMessage": {
                        $stmt = $this->db->prepare("SELECT rM FROM Settings WHERE UID = ?");
                        $stmt->bind_param("i", $uid);
                        $stmt->execute();
                        $stmt->bind_result($rM);
                        while ($stmt->fetch()) {
                            break;
                        }
                        $stmt->close();

                        echo $rM;

                        break;
                    }
                case "RandomMessageSound": {
                        $stmt = $this->db->prepare("SELECT rMSound FROM Settings WHERE UID = ?");
                        $stmt->bind_param("i", $uid);
                        $stmt->execute();
                        $stmt->bind_result($rMSound);
                        while ($stmt->fetch()) {
                            break;
                        }
                        $stmt->close();

                        echo $rMSound;

                        break;
                    }
            }
        }
    }

    function updateSettings($rm, $rmSound) {
        if ($this->IsSignedIn()) {
            $uid = $_SESSION["UID"];

            $stmt = $this->db->prepare("SELECT COUNT(*) FROM Settings WHERE UID = ?");
            $stmt->bind_param("i", $uid);
            $stmt->execute();
            $stmt->bind_result($count);
            while ($stmt->fetch()) {
                break;
            }
            $stmt->close();

            if ($count == 0) {
                $stmt = $this->db->prepare("INSERT INTO Settings (UID, rM, rMSound) VALUES (?, ?, ?)");
                $stmt->bind_param("iii", $uid, $rm, $rmSound);
                $stmt->execute();
                $stmt->close();
            } else {
                $stmt = $this->db->prepare("UPDATE Settings SET rM = ?, rMSound = ? WHERE UID = ?");
                $stmt->bind_param("iii", $rm, $rmSound, $uid);
                $stmt->execute();
                $stmt->close();
            }
        }
    }

    #Post handling

    function post($post, $category, $userId, $photo = NULL) {
        $allowedExts = array("gif", "jpeg", "jpg", "png");
        if (isset($photo)) {
            $path = $_FILES['photo']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);

            $enable = false;
            foreach ($allowedExts as $ex) {
                if ($ext == $ex)
                    $enable = true;
            }

            if (!$enable)
                return;

            $null = NULL;
            $stmt = $this->db->prepare("INSERT INTO Posts (Post, Category, UserId, Image) VALUES (?, ?, ?, ?)");
            $stmt->bind_param("ssib", $post, $category, $userId, $null);
            $stmt->send_long_data(3, file_get_contents($_FILES["photo"]["tmp_name"]));
            $stmt->execute();
            $stmt->close();
        }
        else {
            $stmt = $this->db->prepare("INSERT INTO Posts (Post, Category, UserId) VALUES (?, ?, ?)");
            $stmt->bind_param("ssi", $post, $category, $userId);
            $stmt->execute();
            $stmt->close();
        }
    }

    function getImage($postId, $w = 50) {
        $stmt = $this->db->prepare("SELECT Image FROM Posts WHERE ID = ?");
        $stmt->bind_param("i", $postId);
        $stmt->execute();
        $stmt->bind_result($img);

        while ($stmt->fetch()) {
            header("Content-Type: image/png");

            $source_image = imagecreatefromstring($img);
            $width = imagesx($source_image);
            $height = imagesy($source_image);

            /* find the "desired height" of this thumbnail, relative to the desired width  */
            $desired_height = floor($height * ($w / $width));

            /* create a new, "virtual" image */
            $virtual_image = imagecreatetruecolor($w, $desired_height);

            /* copy source image at a resized size */
            imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $w, $desired_height, $width, $height);

            /* create the physical thumbnail image to its destination */
            imagepng($virtual_image);
            imagedestroy($virtual_image);
        }

        $stmt->close();
    }

    function getMetadata($postId)
    {
        $initQuery = "SELECT Post FROM Posts WHERE ID = ?";
        
        $stmt = $this->db->prepare($initQuery);
        $stmt->bind_param("i", $postId);
        $stmt->execute();
        $stmt->bind_result($post);

        while ($stmt->fetch()) {
            echo "<meta name='description' content='".$post."'/>";
            echo '<meta property="og:description" content="'.$post.'" />';
        }

        $stmt->close();
    }
    
    function getPosts($timeframe, $category, $user = 0) {
        $initQuery = "SELECT Alias, Posts.ID, Posts.UserId, Posts.Post, (Posts.Image IS NOT NULL) AS HasPicture, (SELECT COUNT(*) FROM Stats WHERE PostId = Posts.ID AND Type = 'Like') AS Likes, (SELECT COUNT(*) FROM Stats WHERE PostId = Posts.ID AND UserId = ? AND Type = 'Like') AS Liked, (SELECT COUNT(*) FROM Stats WHERE PostId = Posts.ID AND Type = 'Dislike') AS Dislikes, (SELECT COUNT(*) FROM Stats WHERE PostId = Posts.ID AND UserId = ? AND Type = 'Dislike') AS Disliked FROM Posts INNER JOIN Users ON Posts.UserID = Users.ID ";
        if ($category != "All") {
            $initQuery .= "WHERE Category = '" . $category . "' ";
        }

        switch ($timeframe) {
            case "user": {
                    $query = "Posts.UserId = '" . $user . "' ";
                    $initQuery .= ($category == "All") ? "WHERE " . $query : "AND " . $query;
                    $initQuery .= "ORDER BY PostedWhen DESC ";

                    break;
                }
            case "following": {
                    $userId = 0;
                    if ($this->IsSignedIn())
                        $userId = $_SESSION["UID"];
                    $query = "(Posts.UserId IN (SELECT Following FROM Following WHERE Follower = '" . $userId . "') OR Posts.UserId = '" . $userId . "') ORDER BY PostedWhen DESC ";

                    $initQuery .= ($category == "All") ? "WHERE " . $query : "AND " . $query;

                    break;
                }
            case "top": {
                    $initQuery .= "ORDER BY Likes DESC, PostedWhen DESC ";

                    break;
                }
            case "recent": {
                    $initQuery .= "ORDER BY PostedWhen DESC ";

                    break;
                }
            default: {
                    if (isset($this->postId))
                        $initQuery .= "WHERE Posts.ID = " . $this->postId . " ";

                    break;
                }
        }

        $userId = 0;
        if ($this->IsSignedIn()) {
            $userId = $_SESSION["UID"];
        }

        $postCount = 0;
        //$this->returnedSQLCall("SELECT Alias FROM Users WHERE ID = ?", null, "i", array(&$userId));

        $initQuery .= "LIMIT 25";
        $stmt = $this->db->prepare($initQuery);
        $stmt->bind_param("ii", $userId, $userId);
        $stmt->execute();
        $stmt->bind_result($alias, $id, $userid, $post, $hasImage, $likes, $liked, $dislikes, $disliked);

        while ($stmt->fetch()) {
            $postCount++;
            $likePlaceholder = null;
            $deletePlaceholder = null;
            $dislikePlaceholder = null;

            if ($this->IsSignedIn()) {
                if ($userid === $_SESSION["UID"]) {
                    $deletePlaceholder = '<a class="btn" onClick="DeletePost(this, ' . $id . ', ' . "'" . session_id() . "'" . ', ' . $userid . ')">Delete</a>' . " ・ ";
                } else {
                    $deletePlaceholder = "";
                }

                $likePlaceholder = '<a class="btn" onClick="LikePost(this, ' . $id . ', ' . "'" . session_id() . "'" . ', ' . $_SESSION["UID"] . ')">' . (($liked > 0) ? "Liked" : "Like") . ' (' . $likes . ') </a>';
                $dislikePlaceholder = '<a class="btn" onClick="DislikePost(this, ' . $id . ', ' . "'" . session_id() . "'" . ', ' . $_SESSION["UID"] . ')">' . (($disliked > 0) ? "Disliked" : "Dislike") . ' (' . $dislikes . ') </a>';
            } else {
                $likePlaceholder = "Likes (" . $likes . ")";
                $dislikePlaceholder = "Disikes (" . $dislikes . ")";
            }

            $post = str_replace("\n", "<br />", $post);

            echo
            "<tr>
					<td colspan='3'>
						<table style='width: 100%; background-color: #FAFAFA; border: 4px solid #E8E8E6; padding: 5px'>
							" .
            (
            ($hasImage == 1) ?
                    "<tr>
										<td colspan='3' style='text-align: center; border-bottom: 1px solid'>
										<img src='http://www.randommash.com/Templates/Image.php?id=" . $id . "&w=250' />
										<br />
										<i>" . $post . "</i>
										</td>
									</tr>" :
                    "<tr>
										<td colspan='3' style='text-align: center; border-bottom: 1px solid'>
										" . $post . "
										</td>
									</tr>"
            )
            . "<tr style='font-size: smaller'>
								<td style='text-align: left; width: 33.3333333333%'>
									Posted by <a href='User.php?i=" . $userid . "'>" . $alias . "</a>
								</td>
								<td style='text-align: center; width: 33.3333333333%'>
									
								</td>
								<td style='text-align: right; width: 33.3333333333%;'>"
            . $deletePlaceholder . $likePlaceholder . " ・ " . $dislikePlaceholder . " &nbsp;<a " . ((isset($this->postId)) ? 'id="comments"' : "") . " style='float: right; font-size: x-small; cursor: pointer' onClick=" . '"' . "GetComments(" . $id . ", '" . session_id() . "', this)" . '"' . ">&#8595;</a>
								</td>
							</tr>
							<tr>
								<td colspan='3' style='padding-bottom: 5px'>
								</td>
							</tr>
						</table>
					</td>
            	</tr>";
        }

        if ($postCount == 0) {
            echo
            "<tr>
					<td colspan='3'>
						<table style='width: 100%; background-color: #FAFAFA; border: 4px solid #E8E8E6; padding: 5px'>
							<tr>
								<td colspan='3' style='text-align: center;'>
									There are no posts in this category. :(
								</td>
							</tr>
					</td>
            	</tr>";
        }

        if (isset($this->postId)) {
            unset($this->postId);
            echo "<script>$(document.getElementById('comments')).click();</script>";
        }

        $stmt->close();
    }

    #Like Dislike Handling 

    function disorlikePost($userId, $postId, $dis) {
        $statement = "";
        $stmt = $this->db->prepare("SELECT COUNT(*) FROM Stats WHERE PostId = ? AND Type = ? AND UserId = ?");
        $stmt->bind_param("isi", $postId, $dis, $userId);
        $stmt->execute();
        $stmt->bind_result($count);

        while ($stmt->fetch()) {
            break;
        }

        $stmt->close();

        if ($count == 0)
            $statement = "INSERT INTO Stats (PostId, UserId, Type) VALUES (?, ?, ?)";
        else
            $statement = "DELETE FROM Stats WHERE PostId = ? AND UserId = ? AND Type = ?";
        $stmt = $this->db->prepare($statement);
        $stmt->bind_param("iis", $postId, $userId, $dis);
        $stmt->execute();
        $stmt->close();

        $stmt = $this->db->prepare("SELECT COUNT(*) FROM Stats WHERE PostId = ? AND Type = ?");
        $stmt->bind_param("is", $postId, $dis);
        $stmt->execute();
        $stmt->bind_result($count2);
        while ($stmt->fetch()) {
            break;
        }
        $stmt->close();

        echo $count2;

        $stmt = $this->db->prepare("SELECT UserId, (SELECT Alias FROM Users WHERE ID = ?) AS Alias FROM Posts WHERE ID = ?");
        $stmt->bind_param("ii", $userId, $postId);
        $stmt->execute();
        $stmt->bind_result($posterId, $alias);

        while ($stmt->fetch()) {
            break;
        }

        $stmt->close();

        if ($posterId != $userId && $count == 0) {
            $this->notif->sendNotification($posterId, $alias . " " . strtolower($dis) . "d your post.", $userId, $postId);
            $this->notif->SendNotifications(array($posterId), $alias . " " . strtolower($dis) . "d your post.", array("PostId" => $postId));
        }
    }

    #Comment Handling

    function getPostComments($postId) {
        $count = 0;
        $UID = 0;
        $stmt = $this->db->prepare("SELECT Comments.ID, Comment, Comments.UserID, Alias, Posts.UserId FROM Comments INNER JOIN Users ON Comments.UserID = Users.ID INNER JOIN Posts ON Comments.PostId = Posts.Id WHERE Comments.PostId = ?");
        $stmt->bind_param("i", $postId);
        $stmt->execute();
        $stmt->bind_result($id, $comment, $userId, $alias, $u);

        if ($this->IsSignedIn())
            $UID = $_SESSION["UID"];

        echo "<table>";
        while ($stmt->fetch()) {
            echo
            '
					<tr>
						<td style="font-size: small;">
							' . $comment . ' 
						</td>
						<td style="text-align: right; font-size: small">
							<a style="font-style:italic;" href="/User.php?i=' . $userId . '">~' . $alias . '</a>' . (($UID == $userId || $UID == $u) ? '<a onClick="deleteComment(this, this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode, ' . $UID . ', ' . $postId . ', ' . $id . ', ' . "'" . session_id() . "'" . ')" style="cursor:pointer">・delete</a>' : "") . '
						</td>
					</tr>
				';

            $count++;
        }

        if ($count == 0) {
            echo
            '
					<tr>
						<td>
							<i>No one has commented on this post.<i>
						</td>
					</tr>
				';
        }

        echo "</table>";

        $stmt->close();
    }

    function postComment($post, $postId, $userId) {
        $alias = $this->getAlias($userId, true);
        $stmt = $this->db->prepare("INSERT INTO Comments (Comment, PostId, UserId) VALUES (?, ?, ?)");
        $stmt->bind_param("sii", $post, $postId, $userId);
        $stmt->execute();
        $stmt->close();

        $arr = array();
        $stmt = $this->db->prepare("SELECT UserId FROM Posts WHERE ID = ?");
        $stmt->bind_param("i", $postId);
        $stmt->execute();
        $stmt->bind_result($posterId);

        while ($stmt->fetch()) {
            if ($userId != $posterId) {
                $this->notifAPI->sendNotification($posterId, $alias . " commented on your post.", $userId, $postId);
                $this->notifAPI->SendNotifications(array($posterId), $alias . " commented on your post.", array("PostId" => $postId));
                break;
            }
        }

        $stmt->close();

        $stmt = $this->db->prepare("SELECT DISTINCT UserID FROM Comments WHERE PostID = ? AND UserID != ?");
        $stmt->bind_param("ii", $postId, $userId);
        $stmt->execute();
        $stmt->bind_result($user);

        while ($stmt->fetch()) {
            if ($user != $posterId && $userId != $user)
                array_push($arr, $user);
        }

        $stmt->close();

        foreach ($arr as $str) {
            $this->notifAPI->sendNotification($str, $alias . " also commented on a post.", $userId, $postId);
        }

        if (count($arr) > 0)
            $this->notifAPI->SendNotifications($arr, $alias . " also commented on a post.", array("PostId" => $postId));
    }

    function deleteComment($commentId, $postId, $userId) {
        $stmt = $this->db->prepare("DELETE FROM Comments WHERE Comments.ID = ? AND Comments.PostId = ? AND (Comments.UserId = ? OR (SELECT COUNT(*) FROM Posts WHERE Posts.ID = ? AND UserID = ?) = 1)");
        $stmt->bind_param("iiiii", $commentId, $postId, $userId, $postId, $userId);
        $stmt->execute();
        $stmt->close();
    }

}

class NotificationAPI {

    private $db;

    #MISC Handling

    function __construct() {
        $this->db = new mysqli('localhost', 'randomma', "i'llbeokay14", 'randomma_rm', 8889);
        $this->db->autocommit(TRUE);
    }

    function __destruct() {
        $this->db->close();
    }

    #Notification Handling

    function getMessages($userId) {
        $arr = array();
        $notif = array();

        $stmt = $this->db->prepare("SELECT ID, Data, Notifications.Read FROM Notifications WHERE UID = ? ORDER BY SentWhen DESC");
        $stmt->bind_param("i", $userId);
        $stmt->execute();
        $stmt->bind_result($id, $msg, $read);

        while ($stmt->fetch()) {
            array_push($notif, array("NotificationID" => $id, "Message" => $msg, "Read" => $read));
        }

        $stmt->close();

        $stmt = $this->db->prepare("SELECT Messages.ID, UserId, UserTo, Message, Messages.Read, Alias FROM Messages INNER JOIN Users ON Messages.UserId = Users.ID WHERE (Messages.UserTo = ? OR Messages.UserId = ?) ORDER BY SentWhen DESC");
        $stmt->bind_param("ii", $userId, $userId);
        $stmt->execute();
        $stmt->bind_result($id, $userId, $userTo, $msg, $read, $userAlias);

        while ($stmt->fetch()) {
            array_push($arr, array("MessageID" => $id, "UserFrom" => $userId, "UserFromAlias" => $userAlias, "UserTo" => $userTo, "Message" => $msg, "Read" => $read));
        }

        $stmt->close();
        echo json_encode(array($notif, $arr));
    }

    function getNotifications($userId) {
        $stmt = $this->db->prepare("SELECT ID, Data, Notifications.Read FROM Notifications WHERE UID = ? ORDER BY SentWhen DESC");
        $stmt->bind_param("i", $userId);
        $stmt->execute();
        $stmt->bind_result($id, $msg, $read);

        echo '<table style="width: 100%">';
        $count = 0;
        while ($stmt->fetch()) {
            $count++;
            $msg = json_decode($msg, true);
            $text = $msg["Message"];
            if (isset($msg["UserId"])) {
                $username = (explode(" ", trim($text))[0]);
                $text = str_replace($username, '<a href="/User.php?i=' . $msg["UserId"] . '">' . $username . "</a> ", $text);
            }
            if (isset($msg["PostId"])) {
                $text = str_replace("post", '<a href="/?i=' . $msg["PostId"] . '">post</a>', $text);
            }

            echo "<tr><td>";
            if (!$read)
                echo '<b onmouseover="readNotification(' . $id . ',' . $userId . ', this)">';
            //readNotification(id, userId) 
            echo $text;
            if (!$read)
                echo "</b>";
            echo "</td></tr>";
        }

        if ($count == 0) {
            echo "<tr><td>";
            echo "You do not have any notifications.";
            echo "</td></tr>";
        }

        echo '</table>';

        $stmt->close();
    }

    function readNotifications($msgId, $userId) {
        $stmt = $this->db->prepare("UPDATE Messages SET Messages.Read = 1 WHERE ID = ? AND UserTo = ?");
        $stmt->bind_param("ii", $msgId, $userId);
        $stmt->execute();
        $stmt->close();

        $stmt = $this->db->prepare("UPDATE Notifications SET Notifications.Read = 1 WHERE ID = ? AND UID = ?");
        $stmt->bind_param("ii", $msgId, $userId);
        $stmt->execute();
        $stmt->close();
    }

    function deleteNotifications($msgId, $userId) {
        $stmt = $this->db->prepare("DELETE FROM Messages WHERE ID = ? AND (UserTo = ? OR UserId = ?)");
        $stmt->bind_param("iii", $msgId, $userId, $userId);
        $stmt->execute();
        $stmt->close();

        $stmt = $this->db->prepare("DELETE FROM Notifications WHERE ID = ? AND UID = ?");
        $stmt->bind_param("ii", $msgId, $userId);
        $stmt->execute();
        $stmt->close();
    }

    function sendNotification($userId, $msg, $uid, $postId) {
        $str = json_encode(array("Message" => $msg, "PostId" => $postId, "UserId" => $uid));

        $stmt = $this->db->prepare("SELECT COUNT(*) FROM Notifications WHERE Data = ? AND UID = ?");
        $stmt->bind_param("si", $str, $userId);
        $stmt->execute();
        $stmt->bind_result($count);

        while ($stmt->fetch()) {
            break;
        }

        $stmt->close();

        if ($count == 0) {
            $stmt = $this->db->prepare("INSERT INTO Notifications (Data, UID) VALUES (?, ?)");
            $stmt->bind_param("si", $str, $userId);
            $stmt->execute();
            $stmt->close();
        }
    }

    function postMessage($post, $UID, $userId) {
        $stmt = $this->db->prepare("INSERT INTO Messages (UserId, UserTo, Message) VALUES (?, ?, ?)");
        $stmt->bind_param("iis", $UID, $userId, $post);
        $stmt->execute();
        $stmt->close();

        $stmt = $this->db->prepare("SELECT Alias FROM Users WHERE ID = ?");
        $stmt->bind_param("i", $UID);
        $stmt->execute();
        $stmt->bind_result($alias);

        while ($stmt->fetch()) {
            break;
        }

        $stmt->close();
        $this->SendNotifications(array($userId), $alias . " sent you a message.");
    }

    function getNotificationCount($userId) {
        $stmt = $this->db->prepare("SELECT COUNT(*) FROM Notifications WHERE UID = ? AND Notifications.Read = FALSE");
        $stmt->bind_param("i", $userId);
        $stmt->execute();
        $stmt->bind_result($id);

        while ($stmt->fetch()) {
            echo $id;
        }

        $stmt->close();
    }

    function getMessagesCount($userId) {
        $arr = array();
        $stmt = $this->db->prepare("SELECT (COUNT(*) + (SELECT COUNT(*) FROM Notifications WHERE UID = ? AND Notifications.Read = FALSE)) FROM Messages WHERE UserTo = ? AND Messages.Read = FALSE");
        $stmt->bind_param("ii", $userId, $userId);
        $stmt->execute();
        $stmt->bind_result($id);

        while ($stmt->fetch()) {
            array_push($arr, array("MessageCount" => $id));
        }

        $stmt->close();
        echo json_encode($arr);
    }

    #Device Handling

    function SendNotifications($UIDs, $message, $data = array()) {
        $query = "SELECT UUID, IsBeta, Devices.System, ((SELECT COUNT(*) FROM Messages WHERE UserTo = Devices.UID AND Messages.Read = 0) + (SELECT COUNT(*) FROM Notifications WHERE Notifications.UID = Devices.UID AND Notifications.Read = 0)) AS Badge FROM Devices WHERE UID IN (";
        foreach ($UIDs as $str) {
            $stmt = $this->db->prepare("SELECT COUNT(*) FROM Notifications WHERE Data = ? AND UID = ?");
            $stmt->bind_param("si", json_encode($data), $str);
            $stmt->execute();
            $stmt->bind_result($count);

            while ($stmt->fetch()) {
                break;
            }

            $stmt->close();
            
            if ($count == 0)
                $query .= $str . ",";
        }
        $query = substr($query, 0, -1);
        $query .= ")";

        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $stmt->bind_result($uuid, $isbeta, $system, $badge);

        while ($stmt->fetch()) {
            if ($system == "iOS")
                $this->sendPushNotification($message, $uuid, $badge, $data, $isbeta);
            else
                continue;
        }
        
        $stmt->close();
    }

    function AddiOSDevice($UUID, $UID, $Beta = false) {
        error_log($UUID);
        $stmt = $this->db->prepare("DELETE FROM Devices WHERE UUID = ?");
        $stmt->bind_param("s", $UUID);
        $stmt->execute();
        $stmt->close();

        $stmt = $this->db->prepare("INSERT INTO Devices (UID, UUID, System, IsBeta) VALUES (?, ?, 'iOS', ?)");
        $stmt->bind_param("isi", $UID, $UUID, $Beta);
        $stmt->execute();
        $stmt->close();
    }

    #Android Push Notifications

    function sendGCMNotification($msg, $tokens) {
        $url = 'https://android.googleapis.com/gcm/send';
        $fields = array(
            'registration_ids' => $tokens,
            'data' => $msg,
        );
        // Google Cloud Messaging GCM API Key
        define("GOOGLE_API_KEY", "AIzaSyBIk2CuQlGuFucymbKpWLZNdv_9_S4FQbU");
        $headers = array(
            'Authorization: key=' . GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);

        echo $result;
    }

    #IOS Push notifications

    function sendPushNotification($msg, $token, $badge, $data, $isBeta = false) {
        return;
        $crt = "ssl://gateway.push.apple.com:2195";
        $pem = $_SERVER["DOCUMENT_ROOT"] . "/Web Service/Prod.pem";
        if ($isBeta) {
            $pem = $_SERVER["DOCUMENT_ROOT"] . "/Web Service/Dev.pem";
            $crt = "ssl://gateway.sandbox.push.apple.com:2195";
        }

        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $pem);
        stream_context_set_option($ctx, 'ssl', 'passphrase', "i'llbeokay");

        // Open a connection to the APNS server
        $fp = stream_socket_client($crt, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

        if (!$fp) {
            error_log($err);
            //exit("Failed to connect:" . $err . $errstr . PHP_EOL);
        }

        // Create the payload body
        $body['aps'] = array(
            'alert' => $msg,
            'sound' => 'default',
            'badge' => $badge
        );

        $body['data'] = $data;

        // Encode the payload as JSON
        $payload = json_encode($body);

        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $token) . pack('n', strlen($payload)) . $payload;

        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));
        fclose($fp);
    }

}

?>
