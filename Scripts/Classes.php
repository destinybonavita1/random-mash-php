<?php

$api = new DataAPI();

class DataAPI {
    private $db;
    private $notifAPI;

    function __construct() {
        $this->db = new mysqli('localhost', 'randomma', "i'llbeokay14", 'randomma_rm', 8889);
        $this->db->autocommit(TRUE);
    }

    function __destruct() {
        $this->db->close();
    }

    function register($uuid, $alias) {
        if ($uuid == NULL) {
            error("You did not provide a UUID");
            return;
        }

        $stmt = $this->db->prepare("SELECT ID FROM Users WHERE UUID = ?");
        $stmt->bind_param("s", $uuid);
        $stmt->execute();
        $stmt->bind_result($userId);
        while ($stmt->fetch()) {
            break;
        }

        $stmt->close();

        if ($userId != NULL) {
            echo json_encode(array("UID" => $userId));
            return;
        }

        $stmt = $this->db->prepare('INSERT INTO Users (UUID,Alias) VALUES (?,?)');
        $stmt->bind_param("ss", $uuid, $alias);
        $stmt->execute();
        $stmt->close();

        $stmt = $this->db->prepare('SELECT ID FROM Users WHERE UUID = ?');
        $stmt->bind_param("s", $uuid);
        $stmt->execute();
        $stmt->bind_result($userId);
        while ($stmt->fetch()) {
            break;
        }

        $stmt->close();

        echo json_encode(array("UID" => $userId));
    }

    function readNotifications($msgId, $userId) {
        $stmt = $this->db->prepare("UPDATE Messages SET Messages.Read = 1 WHERE ID = ? AND UserTo = ?");
        $stmt->bind_param("ii", $msgId, $userId);
        $stmt->execute();
        $stmt->close();
    }

    function deleteNotifications($msgId, $userId) {
        $stmt = $this->db->prepare("DELETE FROM Messages WHERE ID = ? AND (UserTo = ? OR UserId = ?)");
        $stmt->bind_param("iii", $msgId, $userId, $userId);
        $stmt->execute();
        $stmt->close();
    }

    function sendNotification($userId, $msg, $postId) {
        $str = json_encode(array("Message" => $msg, "PostId" => $postId));

        $stmt = $this->db->prepare("INSERT INTO Messages (Message, UserId, UserTo) VALUES (?, 0, ?)");
        $stmt->bind_param("si", $str, $userId);
        $stmt->execute();
        $stmt->close();
    }

    function disorlikePost($userId, $postId, $dis) {
        $statement = "";
        $stmt = $this->db->prepare("SELECT COUNT(*) FROM Stats WHERE PostId = ? AND Type = ? AND UserId = ?");
        $stmt->bind_param("isi", $postId, $dis, $userId);
        $stmt->execute();
        $stmt->bind_result($count);

        while ($stmt->fetch()) {
            break;
        }

        $stmt->close();

        if ($count == 0)
            $statement = "INSERT INTO Stats (PostId, UserId, Type) VALUES (?, ?, ?)";
        else
            $statement = "DELETE FROM Stats WHERE PostId = ? AND UserId = ? AND Type = ?";
        $stmt = $this->db->prepare($statement);
        $stmt->bind_param("iis", $postId, $userId, $dis);
        $stmt->execute();
        $stmt->close();

        $stmt = $this->db->prepare("SELECT COUNT(*) FROM Stats WHERE PostId = ? AND Type = ?");
        $stmt->bind_param("is", $postId, $dis);
        $stmt->execute();
        $stmt->bind_result($count2);
        while ($stmt->fetch()) {
            break;
        }
        $stmt->close();

        echo $count2;

        $stmt = $this->db->prepare("SELECT UserId, (SELECT Alias FROM Users WHERE ID = ?) AS Alias FROM Posts WHERE ID = ?");
        $stmt->bind_param("ii", $userId, $postId);
        $stmt->execute();
        $stmt->bind_result($posterId, $alias);

        while ($stmt->fetch()) {
            break;
        }

        $stmt->close();

        if ($posterId != $userId && $count == 0) {
            $this->notifAPI->sendNotification($posterId, $alias . " " . strtolower($dis) . "d your post.", $postId);
            $this->notifAPI->SendNotifications(array($posterId), $alias . " " . strtolower($dis) . "d your post.", array("PostId" => $postId));
        }
    }

    function unlikePost($userId, $postId) {
        $stmt = $this->db->prepare("DELETE FROM Stats WHERE PostId = ? AND UserId = ? AND Type = 'Like'");
        $stmt->bind_param("ii", $postId, $userId);
        $stmt->execute();
        $stmt->close();
    }

    function dislikePost($userId, $postId) {
        $stmt = $this->db->prepare("INSERT INTO Stats (PostId, Type, UserId) VALUES (?, 'Dislike', ?)");
        $stmt->bind_param("ii", $postId, $userId);
        $stmt->execute();
        $stmt->close();
    }

    function undislikePost($userId, $postId) {
        $stmt = $this->db->prepare("DELETE FROM Stats WHERE PostId = ? AND UserId = ? AND Type = 'Dislike'");
        $stmt->bind_param("ii", $postId, $userId);
        $stmt->execute();
        $stmt->close();
    }

    function post($post, $category, $userId) {
        $stmt = $this->db->prepare("INSERT INTO Posts (Post, Category, UserId) VALUES (?, ?, ?)");
        $stmt->bind_param("ssi", $post, $category, $userId);
        $stmt->execute();
        $stmt->close();
    }

    function postMessage($post, $UID, $userId) {
        $stmt = $this->db->prepare("INSERT INTO Messages (UserId, UserTo, Message) VALUES (?, ?, ?)");
        $stmt->bind_param("iis", $UID, $userId, $post);
        $stmt->execute();
        $stmt->close();

        $stmt = $this->db->prepare("SELECT UUID, (SELECT COUNT(*) FROM Messages WHERE Messages.Read = FALSE AND UserTo = ?) + 1 AS Badge, (SELECT Alias FROM Users WHERE ID = ?) AS Alias FROM Users WHERE ID = ?");
        $stmt->bind_param("iii", $userId, $UID, $userId);
        $stmt->execute();
        $stmt->bind_result($uuid, $badge, $alias);

        while ($stmt->fetch()) {
            break;
        }

        $this->sendPushNotification($alias . " messaged you.", $uuid, $badge, array());
    }

    function postComment($post, $postId, $userId) {
        $stmt = $this->db->prepare("INSERT INTO Comments (Comment, PostId, UserId) VALUES (?, ?, ?)");
        $stmt->bind_param("sii", $post, $postId, $userId);
        $stmt->execute();

        while ($stmt->fetch()) {
            break;
        }

        $stmt->close();

        $stmt = $this->db->prepare("SELECT UserId, (SELECT UUID FROM Users WHERE ID = Posts.UserId) AS UUID, (SELECT COUNT(*) FROM Messages WHERE Messages.Read = FALSE AND UserTo = Posts.UserId) + 1 AS Badge FROM Posts WHERE ID = ?");
        $stmt->bind_param("i", $postId);
        $stmt->execute();
        $stmt->bind_result($posterId, $uuid, $badge);

        while ($stmt->fetch()) {
            break;
        }

        $stmt->close();

        if ($posterIdw != $userId) {
            $this->sendNotification($posterId, "Someone commented on your post.", $postId);
            $this->sendPushNotification("Someone commented on your post.", $uuid, $badge, array("PostID" => $postId));
        }
    }

    function deletePost($postId, $userId) {
        $stmt = $this->db->prepare("DELETE FROM Posts WHERE ID = ? AND UserId = ?");
        $stmt->bind_param("ii", $postId, $userId);
        $stmt->execute();
        $stmt->close();
    }

    function deleteComment($commentId, $postId, $userId) {
        $stmt = $this->db->prepare("DELETE FROM Comments WHERE ID = ? AND PostId = ? AND UserId = ?");
        $stmt->bind_param("iii", $commentId, $postId, $userId);
        $stmt->execute();
        $stmt->close();
    }

    function getPost($postId, $userId) {
        if ($postId == NULL || $userId == NULL) {
            error("One of your values is null.");
            return;
        }

        $arr = array();

        $stmt = $this->db->prepare("SELECT ID, UserId, Post, (SELECT COUNT(*) FROM Stats WHERE PostId = Posts.ID AND Type = 'Like') AS Likes, (SELECT COUNT(*) FROM Stats WHERE PostId = Posts.ID AND UserId = ? AND Type = 'Like') AS Liked, (SELECT COUNT(*) FROM Stats WHERE PostId = Posts.ID AND Type = 'Dislike') AS Dislikes, (SELECT COUNT(*) FROM Stats WHERE PostId = Posts.ID AND UserId = ? AND Type = 'Dislike') AS Disliked FROM Posts WHERE UserId = ? AND Posts.ID = ? ORDER BY PostedWhen DESC LIMIT 100");

        $stmt->bind_param("iiii", $userId, $userId, $userId, $postId);
        $stmt->execute();
        $stmt->bind_result($id, $userId, $post, $likes, $liked, $dislikes, $disliked);

        while ($stmt->fetch()) {
            array_push($arr, array("UserId" => $userId, "PostId" => $id, "Post" => $post, "Likes" => $likes, "Liked" => $liked, "Dislikes" => $dislikes, "Disliked" => $disliked));

            //break;
        }

        $stmt->close();
        echo json_encode($arr);
    }

    function getTopPosts($category, $timeframe, $userId) {
        $arr = array();

        $stmt = $this->db->prepare("SELECT ID, UserId, Post, (SELECT COUNT(*) FROM Stats WHERE PostId = Posts.ID AND Type = 'Like') AS Likes, (SELECT COUNT(*) FROM Stats WHERE PostId = Posts.ID AND UserId = ? AND Type = 'Like') AS Liked, (SELECT COUNT(*) FROM Stats WHERE PostId = Posts.ID AND Type = 'Dislike') AS Dislikes, (SELECT COUNT(*) FROM Stats WHERE PostId = Posts.ID AND UserId = ? AND Type = 'Dislike') AS Disliked, (SELECT Alias FROM Users WHERE ID = Posts.UserId) AS Username FROM Posts " . ($category == "All" ? "" : "WHERE Category = '" . $category . "'") . " ORDER BY Likes DESC LIMIT 50");

        $stmt->bind_param("ii", $userId, $userId);
        $stmt->execute();
        $stmt->bind_result($id, $userId, $post, $likes, $liked, $dislikes, $disliked, $alias);

        while ($stmt->fetch()) {
            echo
            "<tr>
					<td colspan='3'>
						<table style='width: 100%; background-color: #FAFAFA; border: 4px solid #E8E8E6; padding: 5px'>
							<tr>
								<td colspan='3' style='text-align: center; border-bottom: 1px solid'>"
            . $post .
            "</td>
							</tr>
							<tr>
								<td style='text-align: left; width: 33.3333333333%'>
									Posted by <a href='User.php?i=" . $userId . "'>" . $alias . "</a>
								</td>
								<td style='text-align: center; width: 33.3333333333%'>
									
								</td>
								<td style='text-align: right; width: 33.3333333333%'>"
            . "Likes (" . $likes . ") <a>Dislikes (" . $dislikes . ")</a>
								</td>
							</tr>
							<tr>
								<td colspan='3' style='padding-bottom: 10px'>
								</td>
							</tr>
						</table>
					</td>
            	</tr>";
            //array_push($arr, array("UserId" => $userId, "PostId" => $id, "Post" => $post, "Likes" => $likes, "Liked" => $liked, "Dislikes" => $dislikes, "Disliked" => $disliked));
            //break;
        }

        $stmt->close();
        //echo json_encode($arr);
    }

    function getRecentPosts($category, $timeframe, $userId) {
        $arr = array();

        $stmt = $this->db->prepare("SELECT ID, UserId, Post, (SELECT COUNT(*) FROM Stats WHERE PostId = Posts.ID AND Type = 'Like') AS Likes, (SELECT COUNT(*) FROM Stats WHERE PostId = Posts.ID AND UserId = ? AND Type = 'Like') AS Liked, (SELECT COUNT(*) FROM Stats WHERE PostId = Posts.ID AND Type = 'Dislike') AS Dislikes, (SELECT COUNT(*) FROM Stats WHERE PostId = Posts.ID AND UserId = ? AND Type = 'Dislike') AS Disliked, (SELECT Alias FROM Users WHERE ID = Posts.UserId) AS Username FROM Posts " . ($category == "All" ? "" : "WHERE Category = '" . $category . "'") . " ORDER BY PostedWhen DESC LIMIT 50");

        $stmt->bind_param("ii", $userId, $userId);
        $stmt->execute();
        $stmt->bind_result($id, $userId, $post, $likes, $liked, $dislikes, $disliked, $alias);

        while ($stmt->fetch()) {
            echo
            "<tr>
					<td colspan='3'>
						<table style='width: 100%; background-color: #FAFAFA; border: 4px solid #E8E8E6; padding: 5px'>
							<tr>
								<td colspan='3' style='text-align: center; border-bottom: 1px solid'>"
            . $post .
            "</td>
							</tr>
							<tr>
								<td style='text-align: left; width: 33.3333333333%'>
									Posted by <a href='User.php?i=" . $userId . "'>" . $alias . "</a>
								</td>
								<td style='text-align: center; width: 33.3333333333%'>
									
								</td>
								<td style='text-align: right; width: 33.3333333333%'>"
            . "Likes (" . $likes . ") <a>Dislikes (" . $dislikes . ")</a>
								</td>
							</tr>
							<tr>
								<td colspan='3' style='padding-bottom: 10px'>
								</td>
							</tr>
						</table>
					</td>
            	</tr>";

            //break;
        }

        $stmt->close();
    }

    function getMyPosts($userId) {
        $stmt = $this->db->prepare("SELECT ID, UserId, Post, (SELECT COUNT(*) FROM Stats WHERE PostId = Posts.ID AND Type = 'Like') AS Likes, (SELECT COUNT(*) FROM Stats WHERE PostId = Posts.ID AND UserId = ? AND Type = 'Like') AS Liked, (SELECT COUNT(*) FROM Stats WHERE PostId = Posts.ID AND Type = 'Dislike') AS Dislikes, (SELECT COUNT(*) FROM Stats WHERE PostId = Posts.ID AND UserId = ? AND Type = 'Dislike') AS Disliked, (SELECT Alias FROM Users WHERE ID = Posts.UserId) AS Username FROM Posts WHERE UserId = ? ORDER BY PostedWhen DESC LIMIT 100");
        $stmt->bind_param("iii", $userId, $userId, $userId);
        $stmt->execute();
        $stmt->bind_result($id, $userId, $post, $likes, $liked, $dislikes, $disliked, $alias);

        while ($stmt->fetch()) {
            echo "<tr>
					<td colspan='3'>
						<table style='width: 100%; background-color: #FAFAFA; border: 4px solid #E8E8E6; padding: 5px'>
							<tr>
								<td colspan='3' style='text-align: center; border-bottom: 1px solid'>"
            . $post .
            "</td>
							</tr>
							<tr>
								<td style='text-align: left'>
									Posted by <a href='User.php?i=" . $userId . "'>" . $alias . "</a>
								</td>
								<td style='text-align: center; width: 33.3333333333%'>
									
								</td>
								<td style='text-align: right; width: 33.3333333333%'>"
            . "Likes (" . $likes . ") <a>Dislikes (" . $dislikes . ")</a>
								</td>
							</tr>
							<tr>
								<td colspan='3' style='padding-bottom: 10px'>
								</td>
							</tr>
						</table>
					</td>
            	</tr>";
        }

        $stmt->close();
    }

    function getPostComments($postId) {
        if ($postId == NULL) {
            error("One of your values is null.");
            return;
        }

        $arr = array();

        $stmt = $this->db->prepare("SELECT ID,Comment,UserId FROM Comments WHERE PostID = ? ORDER BY CommentWhen DESC");
        $stmt->bind_param("i", $postId);
        $stmt->execute();
        $stmt->bind_result($cmtId, $cmt, $userId);

        while ($stmt->fetch()) {
            array_push($arr, array("Comment" => $cmt, "UID" => $userId, "CommentID" => $cmtId));

            //break;
        }

        $stmt->close();
        echo json_encode($arr);
    }

    function getMessagesCount($userId) {
        if ($userId == NULL) {
            error("One of your values is null.");
            return;
        }

        $arr = array();
        $stmt = $this->db->prepare("SELECT COUNT(*) FROM Messages WHERE UserTo = ? AND Messages.Read = FALSE");
        $stmt->bind_param("i", $userId);
        $stmt->execute();
        $stmt->bind_result($id);

        while ($stmt->fetch()) {
            array_push($arr, array("MessageCount" => $id));

            //break;
        }

        $stmt->close();
        echo json_encode($arr);
    }

    function getMessages($userId) {
        if ($userId == NULL) {
            error("One of your values is null.");
            return;
        }

        $arr = array();
        $notif = array();

        $stmt = $this->db->prepare("SELECT ID, UserId, (SELECT Alias FROM Users WHERE ID = Messages.UserId) AS Alias, UserTo, Message, Messages.Read FROM Messages WHERE (UserTo = ? OR UserId = ?) ORDER BY SentWhen DESC");
        $stmt->bind_param("ii", $userId, $userId);
        $stmt->execute();
        $stmt->bind_result($id, $userId, $userAlias, $userTo, $msg, $read);

        while ($stmt->fetch()) {
            if ($userId == 0) {
                array_push($notif, array("NotificationID" => $id, "Message" => $msg, "Read" => $read));
            } else {
                array_push($arr, array("MessageID" => $id, "UserFrom" => $userId, "UserFromAlias" => $userAlias, "UserTo" => $userTo, "Message" => $msg, "Read" => $read));
            }
        }

        $stmt->close();
        echo json_encode(array($notif, $arr));
    }

    function categoriesNew() {
        $cats = array("All", "Conversations", "Feels", "Punny Jokes", "Pickup Lines", "Conversation Starters");

        foreach ($cats as $str) {
            echo "<option>" . $str . "</option>";
        }
    }

    function error($err) {
        $arr = array("error" => $err);
        echo json_encode($arr);
    }

    function sendPushNotification($msg, $token, $badge, $data) {
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', 'ck.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', "i'llbeokay");

        // Open a connection to the APNS server
        $fp = stream_socket_client(
                'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);

        echo 'Connected to APNS' . PHP_EOL;

        // Create the payload body
        $body['aps'] = array(
            'alert' => $msg,
            'sound' => 'default',
            'badge' => $badge
        );
        $body['data'] = $data;

        // Encode the payload as JSON
        $payload = json_encode($body);

        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $token) . pack('n', strlen($payload)) . $payload;

        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));

//            echo $result;
//            
//            if (!$result)
//                echo 'Message not delivered' . PHP_EOL;
//            else
//                echo 'Message successfully delivered' . PHP_EOL;
        // Close the connection to the server
        fclose($fp);
    }

}

?>