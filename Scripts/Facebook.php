<?php

if (!isset($api2))
    include ("Class.php");

require $_SERVER['DOCUMENT_ROOT']. '/vendor/autoload.php';

class FBHandler {

    private $db;
    public $facebook;
    private $redirectUri;

    function __construct() {
        $this->redirectUri = 'http://randommash.com/Login.php';
        $this->facebook = new Facebook(array(
            'appId' => '1398154103789100',
            'secret' => '30d2dcda1d558f321e649e830891d484',
        ));

        $this->db = new mysqli('localhost', 'randomma', "i'llbeokay14", 'randomma_rm', 8889);
        $this->db->autocommit(TRUE);
    }

    function __destruct() {
        $this->db->close();
    }

    function Login($UID) {
        $id = 0;

        $stmt = $this->db->prepare("SELECT UID FROM Facebook WHERE FacebookID = ?");
        $stmt->bind_param("i", $UID);
        $stmt->execute();
        $stmt->bind_result($id);
        while ($stmt->fetch()) {
            $id = $id;
        }
        $stmt->close();

        $_SESSION["UID"] = $id;
        echo "<script>window.location = '/'</script>";
    }

    function Register($username, $UID) {
        if (!$this->CheckExists($UID)) {
            $pass = $UID * (time() - 821996) + (112014);
            $UID = (int) $UID;

            if (!isset($api2))
                $api2 = new DataAPIM();

            $id = $api2->Register($username, $pass, false);
            $_SESSION["UID"] = $id;

            $stmt = $this->db->prepare("INSERT INTO Facebook (UID, FacebookID) VALUES (?, ?)");
            $stmt->bind_param("ii", $id, $UID);
            $stmt->execute();
            $stmt->close();

            error_log("ReturnedId: " . $id);
            error_log("Username: " . $username . " UID: " . $UID);
        }
        else {
            error_log("Exists");
            echo "exists";
        }
    }

    function GetSignin() {
        $loginUrl = $this->facebook->getLoginUrl();
        echo '<input type="button" onclick="window.location=' . "'" . $loginUrl . "'" . '" value="Login w/Facebook" />';
    }

    function CheckCallback() {
        if ($this->facebook->getUser() != 0) {
//            echo $this->facebook->getUser();
//            echo "<br />".$this->facebook->api("/me")["name"];

            return true;
        }

        return false;
    }

    function CheckExists($UID = null) {
        if ($this->CheckCallback() || isset($UID)) {
            if ($UID == null)
                $UID = $this->facebook->getUser();

            $stmt = $this->db->prepare("SELECT COUNT(*) FROM Facebook WHERE FacebookID = ?");
            $stmt->bind_param("i", $UID);
            $stmt->execute();
            $stmt->bind_result($count);
            while ($stmt->fetch()) {
                break;
            }
            $stmt->close();

            error_log("Count doesn't exist: " . $count);
            if ($count > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

}
