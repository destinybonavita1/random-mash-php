// JavaScript Document
function DeletePost(obj, id, sessionId, uid)
{
    if (window.confirm("Are you sure you want to delete this post?"))
    {
        obj.parentNode.parentNode.parentNode.parentNode.style.display = 'none';

        $.ajax(
                {
                    url: '/Scripts/Postback.php',
                    data: {'postId': id, 'sessionId': sessionId, 'userId': uid, 'method': 'delete'},
                    type: 'POST',
                    success: function(data) {
                        console.log(data);
                    },
                    error: function(data) {
                        console.log("Failed");
                    }
                });
    }
}

function LikePost(obj, id, sessionId, uid)
{
    $.ajax(
            {
                url: '/Scripts/Postback.php',
                data: {'postId': id, 'sessionId': sessionId, 'userId': uid, 'method': 'like'},
                type: 'POST',
                success: function(data) {
                    obj.innerHTML = "Likes (" + data + ")";
                },
                error: function(data) {
                    console.log("Failed");
                }
            });
}

function DislikePost(obj, id, sessionId, uid)
{
    $.ajax(
            {
                url: '/Scripts/Postback.php',
                data: {'postId': id, 'sessionId': sessionId, 'userId': uid, 'method': 'dislike'},
                type: 'POST',
                success: function(data) {
                    obj.innerHTML = "Dislikes (" + data + ")";
                },
                error: function(data) {
                    console.log("Failed");
                }
            });
}

function UpdateSettings(sessionId, uid, rm, rmSound)
{
    $.ajax(
            {
                url: '/Scripts/Postback.php',
                data: {'rmSelect': rm, 'rmCheck': rmSound, 'sessionId': sessionId, 'userId': uid, 'method': 'updateSettings'},
                type: 'POST',
                success: function(data) {

                },
                error: function(data) {
                    console.log("Failed");
                }
            });
}

function SearchUsers(users) {
    $.ajax(
    {
        url: '/Scripts/Postback.php',
        data: {'method': 'searchUser', 'user' : users },
        type: 'POST',
        success: function(data) {
            document.getElementsByName("results")[0].innerHTML = data;
        },
        error: function(data) {
            console.log("Failed");
        }
    });
}

function GetComments(postId, sessionId, element)
{
    var table = element.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode;
    var tableIndex = element.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.rowIndex;

    element.disabled = "disabled";
    if (element.innerHTML === '↓')
    {
        var row = table.insertRow(tableIndex + 1);
        $(row).hide();

        var cell = row.insertCell(0);
        cell.colSpan = 3;

        $.get("../Templates/Comments.php?i=" + postId + "&sessionId=" + sessionId, function(data) {
            cell.innerHTML += data;
        });

        element.innerHTML = '↑';
        $(row).show("slow", null, function() {
            //$(rowPost).show("slow");	
        });
    }
    else
    {
        var row = table.rows[tableIndex + 1];
        $(row).hide("slow", null, function() {
            table.deleteRow(tableIndex + 1);
        });

        element.innerHTML = '↓';
    }
    element.disabled = null;
}

function SendComment(event, inputField, postId, UID, sessionId)
{
    if (event.keyCode === 13 && inputField.value !== "" && inputField.value.replace(" ", "").length > 0)
    {
        $.ajax(
                {
                    url: '/Scripts/Postback.php',
                    data: {'postId': postId, 'sessionId': sessionId, 'userId': UID, 'method': 'comment', 'commentTxt': inputField.value},
                    type: 'POST',
                    success: function(data) {
                        var cell = inputField.parentNode.parentNode.parentNode.parentNode;
                        $.get("../Templates/Comments.php?i=" + postId, function(data) {
                            cell.innerHTML = data;
                        });
                    },
                    error: function(data) {
                        console.log("Failed");
                    }
                });
    }
}

function deleteComment(obj, table, UserId, PostId, CommentId, SessionId)
{
    var row = obj.parentNode.parentNode;
    $(row).hide("slow", null, function() {
        $.ajax(
                {
                    url: '/Scripts/Postback.php',
                    data: {'postId': PostId, 'commentId': CommentId, 'sessionId': SessionId, 'userId': UserId, 'method': 'deleteComment'},
                    type: 'POST',
                    success: function(data) {
                        var cell = obj.parentNode;
                        $.get("../Templates/Comments.php?i=" + PostId, function(data) {
                            table.innerHTML = data;
                        });
                    },
                    error: function() {
                        console.log("Failed");
                    }
                });
    });
}

function createFBUsername(uid) {
    var un = prompt("Please pick a username.");
    if (un.replace(" ", "").length === 0 && un !== null)
    {
        createFBUsername();
    }
    else if (un === null)
    {
        return;
    }
    else {
        $.ajax(
                {
                    url: '/Scripts/Postback.php',
                    data: {'username': un, 'UID': uid, 'method': 'addFacebookUsername'},
                    type: 'POST',
                    success: function(data) {
                        if (data !== "exists") {
                            window.location = "/";
                        }
                        else {
                            window.location = "Login.php?d=exists";
                        }
                    },
                    error: function() {
                        console.log("Failed");
                        return "failed";
                    }
                });
    }
}

function followUnfollow(follower, follow, sessionId, anchor) {
    $.ajax(
            {
                url: '/Scripts/Postback.php',
                data: {'follower': follower, 'follow': follow, 'sessionId': sessionId, 'method': 'followUnfollow'},
                type: 'POST',
                success: function(data) {
                    anchor.innerHTML = data;
                },
                error: function() {
                    console.log("Failed");
                }
            });
}

//Notifications

var notifCount;
function checkNotifications(userId) {
    $.ajax(
            {
                url: '/Scripts/Postback.php',
                data: {'userId': userId, 'method': 'getNotificationCount'},
                type: 'POST',
                success: function(data) {
                    if (notifCount < data && notifCount !== null && document.getElementById("notificationCounter").parentNode.id !== "open") {
                        openNotifications(document.getElementById("notificationCounter").parentNode, userId);
                    }
                    
                    notifCount = data;
                    document.getElementById("notificationCounter").innerHTML = data;
                },
                error: function() {
                    console.log("Failed");
                }
            });
}

function openNotifications(view, id) {
    if (view.id === "open") {
        var notifView = view.parentNode.childNodes[1];
        notifView.id = "";
        notifView.innerHTML = "";
        
        view.id = "";
    }
    else
    {
        var notifView = view.parentNode.childNodes[1];
        notifView.id = "notifications";
        view.id = "open";
        
        $.ajax(
            {
                url: '/Scripts/Postback.php',
                data: {"userId": id, 'method': 'checkNotifications'},
                type: 'POST',
                success: function(data) {
                    notifView.innerHTML = data;
                    $(notifView).scrollTop(1000000);
                },
                error: function() {
                    console.log("Failed");
                }
            });
    }
}

function readNotification(id, userId, notif) 
{
    var n = notif.innerHTML;
    var p = notif.parentNode;
    
    $(notif).remove();
    p.innerHTML = n;
    $.ajax(
            {
                url: '/Scripts/Postback.php',
                data: {"userId": userId, "id" : id, 'method': 'readNotification'},
                type: 'POST',
                success: function() {
                    checkNotifications(userId);
                },
                error: function() {
                    console.log("Failed");
                }
            });
}