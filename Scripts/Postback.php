<?php

include("Class.php");
include("Classes.php");
include("Facebook.php");

if (isset($_POST["method"])) {
    $method = $_POST["method"];

    switch ($method) {
        case "delete": {
                session_id($_POST["sessionId"]);
                session_start();
                if ($_POST["userId"] == $_SESSION["UID"])
                    $api->deletePost($_POST["postId"], $_POST["userId"]);

                break;
            }
        case "deleteComment": {
                session_id($_POST["sessionId"]);
                session_start();
                if ($_POST["userId"] == $_SESSION["UID"]) {
                    $api2->deleteComment($_POST["commentId"], $_POST["postId"], $_POST["userId"]);
                }

                break;
            }
        case "like": {
                session_id($_POST["sessionId"]);
                session_start();
                if ($_POST["userId"] == $_SESSION["UID"]) {
                    $api2->disorlikePost($_POST["userId"], $_POST["postId"], "Like");
                }

                break;
            }
        case "dislike": {
                session_id($_POST["sessionId"]);
                session_start();
                if ($_POST["userId"] == $_SESSION["UID"]) {
                    $api2->disorlikePost($_POST["userId"], $_POST["postId"], "Dislike");
                }

                break;
            }
        case "updateSettings": {
                session_id($_POST["sessionId"]);
                session_start();
                if ($_POST["userId"] == $_SESSION["UID"]) {
                    $api2->updateSettings($_POST["rmSelect"], $_POST["rmCheck"]);
                }

                break;
            }
        case "comment": {
                session_id($_POST["sessionId"]);
                session_start();
                if ($_POST["userId"] == $_SESSION["UID"]) {
                    $api2->postComment($_POST["commentTxt"], $_POST["postId"], $_POST["userId"]);
                }

                break;
            }
        case "addFacebookUsername": {
                $fb = new FBHandler();
                $fb->Register($_POST["username"], $_POST["UID"]);
                break;
            }
        case "followUnfollow": {
            session_id($_POST["sessionId"]);
            session_start();
            if ($_POST["follower"] == $_SESSION["UID"]) {
                $api2->followUnfollow($_POST["follower"], $_POST["follow"]);
            }
            
            if ($api2->isFollowing($_POST["follower"], $_POST["follow"])) {
                echo "Unfollow";
            }
            else {
                echo "Follow";
            }
            
            break;
        }
        case "checkNotifications": {
            $notifAPI->getNotifications($_POST["userId"]);
            break;
        }
        case "getNotificationCount": {
            $notifAPI->getNotificationCount($_POST["userId"]);
            break;
        }
        case "readNotification": {
            $notifAPI->readNotifications($_POST["id"], $_POST["userId"]);
            break;
        }
        case "searchUser": {
            $api2->searchUsers($_POST["user"]);
            break;
        }
    }
} else {
    echo "No method included.";
}
?>