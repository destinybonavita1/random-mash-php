<?php
$title = "Search for a user.";
include ("Templates/Head.php");
?>
<div id="body">
    <table style="width:50%; margin: auto">
        <tr>
            <td>
                <input type="text" onkeyup="SearchUsers(this.value)" name="user" style="width: 90%" placeholder="Search for a username..." />
                <input type="submit" value="Search" />
            </td>
        </tr>
        <tr>
            <td name="results">
                
            </td>
        </tr>
    </table>
</div>
<script>
    SearchUsers("Why haven't you searched one yet?");
</script>