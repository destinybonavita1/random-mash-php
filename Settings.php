<?php
$title = "Welcome to Random Mash!";
include ("Templates/Head.php");
?>
<div id="body">
    <table style="width:35%; margin: auto; background-color: #FAFAFA; border: 4px solid #E8E8E6; padding: 5px">
        <caption>
            Notification Settings
        </caption>
        <tr>
            <td>
                Random Messages:
            </td>
            <td align="right">
                <select name="rmSelect">
                    <option>Daily</option>
                    <option>Hourly</option>
                    <option>None</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                Random Messages:
            </td>
            <td align="right">
                Sound <input name="rmCheck" type="checkbox" />
            </td>
        </tr>
        <!--<tr>
            <td>
                Private Notifications:
            </td>
            <td align="right">
                <input name="pnCheck" type="checkbox" />
            </td>
        </tr>-->
    </table>
    <br />
    <!--<table style="width:35%; margin: auto; background-color: #FAFAFA; border: 4px solid #E8E8E6; padding: 5px">
        <caption>
            Device Settings
        </caption>
        <tr>
            <td align="center" style="width: 33%">
                <u>Device Name</u>
            </td>
            <td align="center" style="width: 33%">
                <u>OS</u>
            </td>
            <td colspan="2" align="center" style="width: 33%">
                <u>Delete</u>
            </td>
        </tr>
        <tr style="font-size: small">
            <td>
                Device Name
            </td>
            <td>
            iOS / Android
            </td>
            <td align="center">
                Enabled <input type="checkbox" checked="true" />
            </td>
            <td align="center">
                <a href="#">Delete</a>
            </td>
        </tr>
        <tr style="font-size: small">
            <td>
                Device Name
            </td>
            <td>
            iOS / Android
            </td>
            <td align="center">
                Enabled <input type="checkbox" checked="true" />
            </td>
            <td align="center">
                <a href="#">Delete</a>
            </td>
        </tr>
        <tr style="font-size: small">
            <td>
                Device Name
            </td>
            <td>
            iOS / Android
            </td>
            <td align="center">
                Enabled <input type="checkbox" checked="true" />
            </td>
            <td align="center">
                <a href="#">Delete</a>
            </td>
        </tr>
        <tr style="font-size: small">
            <td>
                Device Name
            </td>
            <td>
            iOS / Android
            </td>
            <td align="center">
                Enabled <input type="checkbox" checked="true" />
            </td>
            <td align="center">
                <a href="#">Delete</a>
            </td>
        </tr>
        <tr style="font-size: small">
            <td>
                Device Name
            </td>
            <td>
            iOS / Android
            </td>
            <td align="center">
                Enabled <input type="checkbox" checked="true" />
            </td>
            <td align="center">
                <a href="#">Delete</a>
            </td>
        </tr>
        <tr style="font-size: small">
            <td>
                Device Name
            </td>
            <td>
            iOS / Android
            </td>
            <td align="center">
                Enabled <input type="checkbox" checked="true" />
            </td>
            <td align="center">
                <a href="#">Delete</a>
            </td>
        </tr>
    </table>-->
    <table style="width:35%; margin: auto; background-color: #FAFAFA; border: 4px solid #E8E8E6; padding: 5px">
        <tr>
            <td align="center">
                <input onClick="Update()" type="submit" value="Save" />
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        function Update()
        {
            UpdateSettings("<?php echo session_id() ?>", <?php echo $_SESSION["UID"] ?>, document.getElementsByName("rmSelect")[0].selectedIndex, Number(document.getElementsByName("rmCheck")[0].checked));
        }

        document.getElementsByName("rmCheck")[0].checked = Boolean(<?php $api2->getSetting("RandomMessageSound"); ?>);
        document.getElementsByName("rmSelect")[0][<?php $api2->getSetting("RandomMessage"); ?>].selected = true;
    </script>
</div>