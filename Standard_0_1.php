<?php

include("Web Service/Main.php");
header('Content-type: text/html; charset=utf-8');

if (!isset($_POST["METHOD"]))
{
    echo "You forgot something: ".json_encode($_POST);
    
    return;
}
else {
    $method = $_POST["METHOD"];
error_log($_POST["METHOD"]);
    
    switch ($method) {
        #MISC Handling
        case "categoriesNew": {
                $newAPI->categoriesNew();

                break;
            }

        #User Handling
        case "Login": {
                $newAPI->Login($_POST["Username"], $_POST["Password"]);

                break;
            }
        case "Register": {
                $newAPI->Register($_POST["Username"], $_POST["Password"]);

                break;
            }
        case "Search": {
            $newAPI->searchAlias($_POST["alias"], $_POST["UID"]);
            
            break;
        }
        case "FollowUnfollow": {
            $newAPI->followUnfollow($_POST["Follower"], $_POST["Following"]);
            
            break;
        }
        case "IsFollowing": {
            if ($newAPI->isFollowing($_POST["Follower"], $_POST["Following"]))
                echo json_encode(array ("Following" => true));
            else
                echo json_encode(array ("Following" => false));
            
            break;
        }

        #Post Handling
        case "post": {
                $newAPI->post($_POST["Post"], $_POST["Category"], $_POST["UID"], ((isset($_POST["Image"])) ? $_POST["Image"] : NULL));

                break;
            }
        case "getPost": {
                $newAPI->getPost($_POST["PostId"], $_POST["UID"]);

                break;
            }
        case "getFollowingPosts": {
                $newAPI->getPosts("following", $_POST["Category"], $_POST["UID"]);

                break;
            }
        case "getTopPosts": {
                $newAPI->getPosts("top", $_POST["Category"], $_POST["UID"]);

                break;
            }
        case "getRecentPosts": {
                $newAPI->getPosts("recent", $_POST["Category"], $_POST["UID"]);

                break;
            }
        case "getMyPosts": {
                $newAPI->getPosts("user", $_POST["Category"], $_POST["UID"], $_POST["UserId"]);
                ;

                break;
            }
        case "getTheirPosts": {
                $newAPI->getPosts("user", $_POST["Category"], $_POST["UID"], $_POST["UserId"]);
                ;

                break;
            }
        case "getWorstPosts":
        {
            $newAPI->getPosts("worst", $_POST["Category"], $_POST["UID"], $_POST["UserId"]);
                ;
            
            break;
        }
        case "deletePost": {
                $newAPI->deletePost($_POST["PostId"], $_POST["UID"]);

                break;
            }

        #Like Dislike Handling 
        case "getLikes": {
            $newAPI->getLiked($_POST["PostId"], $_POST["UID"]);
            
            break;
        }
        case "getDislikes": {
            $newAPI->getDisliked($_POST["PostId"], $_POST["UID"]);
            
            break;
        }
        case "likePost": {
                $newAPI->disorlikePost($_POST["UID"], $_POST["PostId"], "Like");

                break;
            }
        case "unlikePost": {
                $newAPI->disorlikePost($_POST["UID"], $_POST["PostId"], "Like");

                break;
            }
        case "dislikePost": {
                $newAPI->disorlikePost($_POST["UID"], $_POST["PostId"], "Dislike");

                break;
            }
        case "undislikePost": {
                $newAPI->disorlikePost($_POST["UID"], $_POST["PostId"], "Dislike");

                break;
            }
            
        #Comment Handling
        case "getPostComments": {
                $newAPI->getPostComments($_POST["PostId"]);

                break;
            }
        case "postComment": {
                $newAPI->postComment($_POST["Post"], $_POST["PostId"], $_POST["UID"]);

                break;
            }
        case "deleteComment": {
                $newAPI->deleteComment($_POST["CommentID"], $_POST["PostId"], $_POST["UID"]);

                break;
            }

        #Message Handling
        case "getMessages": {
                $notifAPI->getMessages($_POST["UID"]);

                break;
            }
        case "getMessagesCount": {
                $notifAPI->getMessagesCount($_POST["UID"]);

                break;
            }
        case "postMessage": {
                $notifAPI->postMessage($_POST["Post"], $_POST["UID"], $_POST["Profile"]);

                break;
            }

        #Notifications
        case "AddiOSDevice": {
                $uid = (isset($_POST["UID"]) ? $_POST["UID"] : NULL);
                $notifAPI->AddiOSDevice($_POST["UUID"], $uid, (isset($_POST["Debug"])) ? 1 : 0);

                error_log("Adding iOS Device: ".$_POST["UUID"]);
                
                break;
            }
        case "sendNotification": {
                $notifAPI->sendNotification($_POST["UID"], $_POST["Msg"], $_POST["PostId"]);

                break;
            }
        case "readNotifications": {
                $notifAPI->readNotifications($_POST["MsgID"], $_POST["UID"]);

                break;
            }
        case "deleteNotifications": {
                $notifAPI->deleteNotifications($_POST["MsgID"], $_POST["UID"]);

                break;
            }
        case "sendPushNotification": {
                $notifAPI->SendNotifications(array($_POST["UIDs"]), $_POST["Msg"]);

                break;
            }
        case "sendRandomPost": {
            if (isset($_POST["UID"]))
            {
                $notifAPI->sendRandomPost($_POST["UID"]);
            }
            else
                $notifAPI->sendRandomPost();

                break;
            }
        case "getNotifications": {
            $notifAPI->getNotifications($_POST["UID"]);
            
            break;
        }
        default: {
                break;
            }
    }
}

?>