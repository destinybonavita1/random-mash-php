<?php

if ($api2->IsSignedIn()) {
    $str = basename($_SERVER['PHP_SELF']);
    if ($str == "User.php") {
        if (isset($_GET["i"])) {
            if ($_GET["i"] != $_SESSION["UID"]) {
                return;
            }

            $str = $str . "?i=" . $_GET["i"];
        } else {
            return;
        }
    }

    echo
    '
		<form method="POST" action="' . $str . '" enctype="multipart/form-data">
			<table style="border: 1px solid">
				<tr>
					<td>
						<textarea name="post" style="resize: none; width: 100%; height: 80px; background-color: transparent; border: none" placeholder="Type your post here..."></textarea><br />
						<select name="postcatDrop">'
    . $api2->categoriesArray() .
    '</select>
						<input type="file" name="photo" accept="image/*" />
						<input type="submit" value="Post" style="float: right;" />
						<input type="hidden" value="' . $_SESSION["UID"] . '" name="UID" />
					</td>
				</tr>
			</table>
		</form>
		';

    if ((isset($_POST["post"]) || $_FILES["photo"]["name"] != NULL) && isset($_POST["postcatDrop"])) {
        if ($_FILES["photo"]["name"] != NULL) {
            $api2->post($_POST["post"], $_POST["postcatDrop"], $_SESSION["UID"], $_FILES["photo"]);
        }
        else if (strlen($_POST["post"]) > 0)
        {
            $api2->post($_POST["post"], $_POST["postcatDrop"], $_SESSION["UID"]);
        }
        echo "<script>document.location = '/?t=recent';</script>";
    }
}
?>