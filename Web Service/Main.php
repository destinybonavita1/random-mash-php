<?php

$mainMethod;
$newAPI = new DBAPI();
$notifAPI = new NotificationAPI();
header('Content-type: text/html; charset=utf-8');

if (isset($_POST["METHOD"])) {
    $mainMethod = $_POST["METHOD"];

    switch ($mainMethod) {
        default:
            break;
    }
}

class DBAPI {

    private $db;
    private $notifAPI;

    #MISC Handling

    function categoriesNew() {
        echo json_encode(array("All", "Anti Jokes", "Bad Insults", "Burns", "Cringe Worthy", "Conversations", "Conversation Starters", "Dirty Minded", "Emoji Stories", "Feels", "#ICant", "Icons", "Punny Jokes", "Pickup Lines", "Random", "Rhetorical questions", "Unneeded Hashtags", "Yo Momma"));
    }

    function __construct() {
        $this->db = new mysqli('localhost', 'randomma', “password”, 'randomma_rm', 8889);
        $this->db->autocommit(TRUE);

        $this->notifAPI = new NotificationAPI;
    }

    function __destruct() {
        $this->db->close();
    }

    function error($err) {
        $arr = array("error" => $err);
        echo json_encode($arr);
    }

    #User Handling

    function Login($user, $pass) {
        $stmt = $this->db->prepare("SELECT COUNT(*) FROM Users WHERE Alias = ?");
        $stmt->bind_param("s", $user);
        $stmt->execute();
        $stmt->bind_result($count);
        while ($stmt->fetch()) {
            break;
        }
        $stmt->close();

        if ($count == 0)
            $this->error("That account doesn't exist.");
        else {
            $stmt = $this->db->prepare("SELECT ID FROM Users WHERE Alias = ? AND Password = ?");
            $stmt->bind_param("ss", $user, $pass);
            $stmt->execute();
            $stmt->bind_result($UID);
            while ($stmt->fetch()) {
                break;
            }
            $stmt->close();

            if ($UID != NULL) {
                echo json_encode(array("UID" => $UID));
            } else
                $this->error("Your username or password is incorrect.");
        }
    }

    function AddPass($UID, $pass) {
        $stmt = $this->db->prepare("SELECT COUNT(*) FROM Users WHERE ID = ? AND Password IS NOT NULL");
        $stmt->bind_param("i", $UID);
        $stmt->execute();
        $stmt->bind_result($count);
        while ($stmt->fetch()) {
            break;
        }

        $stmt->close();

        if ($count > 0)
            return;

        $stmt = $this->db->prepare("UPDATE Users SET Password = ? WHERE ID = ?");
        $stmt->bind_param("si", $pass, $UID);
        $stmt->execute();
        $stmt->close();
    }

    function Register($user, $pass) {
        $stmt = $this->db->prepare("SELECT COUNT(*) FROM Users WHERE Alias = ?");
        $stmt->bind_param("s", $user);
        $stmt->execute();
        $stmt->bind_result($count);
        while ($stmt->fetch()) {
            break;
        }
        $stmt->close();

        if ($count > 0)
            $this->error("That username is taken.");
        else {
            $stmt = $this->db->prepare("INSERT INTO Users (Alias, Password) VALUES (?, ?)");
            $stmt->bind_param("ss", $user, $pass);
            $stmt->execute();
            $stmt->close();

            $stmt = $this->db->prepare("SELECT ID FROM Users WHERE Alias = ?");
            $stmt->bind_param("s", $user);
            $stmt->execute();
            $stmt->bind_result($UID);
            while ($stmt->fetch()) {
                break;
            }
            $stmt->close();

            echo json_encode(array("UID" => $UID));
        }
    }

    function getAlias($userId, $return = false) {
        $stmt = $this->db->prepare("SELECT Alias FROM Users WHERE ID = ?");
        $stmt->bind_param("i", $userId);
        $stmt->execute();
        $stmt->bind_result($alias);
        while ($stmt->fetch()) {
            break;
        }

        $stmt->close();

        if (!$return)
            echo $alias;
        else
            return $alias;
    }

    function searchAlias($alias, $userId = 0) {
        $results = array();
        $snd = "%" . $alias . "%";
        $stmt = $this->db->prepare("SELECT Alias, ID, (SELECT COUNT(*) FROM Posts WHERE UserID = Users.ID) AS Count, (SELECT COUNT(*) FROM Following WHERE Following = Users.ID AND Follower = ?) AS Following FROM Users WHERE Alias LIKE ?");
        $stmt->bind_param("is", $userId, $snd);
        $stmt->execute();
        $stmt->bind_result($a, $id, $postCount, $following);

        while ($stmt->fetch()) {
            array_push($results, array("Alias" => $a, "UserId" => $id, "PostCount" => $postCount, "Following" => $following));
        }

        $stmt->close();
        echo json_encode($results);
    }

    function followUnfollow($follower, $following) {
        if ($this->isFollowing($follower, $following)) {
            $stmt = $this->db->prepare("DELETE FROM Following WHERE Follower = ? AND Following = ?");
            $stmt->bind_param("ii", $follower, $following);
            $stmt->execute();
            $stmt->close();
        } else {
            $stmt = $this->db->prepare("INSERT INTO Following (Follower, Following) VALUES (?, ?)");
            $stmt->bind_param("ii", $follower, $following);
            $stmt->execute();
            $stmt->close();

            $stmt = $this->db->prepare("SELECT Alias FROM Users WHERE ID = ?");
            $stmt->bind_param("i", $follower);
            $stmt->execute();
            $stmt->bind_result($alias);

            while ($stmt->fetch()) {
                break;
            }

            $stmt->close();

            $this->notifAPI->sendNotification($following, $alias . " is now following you.", $follower, NULL);
            $this->notifAPI->SendNotifications(array($following), $alias . " is now following you.", array("UserId" => $follower));
        }
    }

    function isFollowing($follower, $following) {
        $stmt = $this->db->prepare("SELECT COUNT(*) FROM Following WHERE Follower = ? AND Following = ?");
        $stmt->bind_param("ii", $follower, $following);
        $stmt->execute();
        $stmt->bind_result($count);
        while ($stmt->fetch()) {
            break;
        }
        $stmt->close();
        return ($count > 0) ? true : false;
    }

    #Post Handling

    function post($post, $category, $userId, $img = NULL) {
        if (isset($img)) {
            $null = NULL;
            $stmt = $this->db->prepare("INSERT INTO Posts (Post, Category, UserId, Image) VALUES (?, ?, ?, ?)");
            $stmt->bind_param("ssib", $post, $category, $userId, $null);
            $stmt->send_long_data(3, base64_decode($img));
            $stmt->execute();
            $stmt->close();
        } else {
            $stmt = $this->db->prepare("INSERT INTO Posts (Post, Category, UserId) VALUES (?, ?, ?)");
            $stmt->bind_param("ssi", $post, $category, $userId);
            $stmt->execute();
            $stmt->close();
        }
    }

    function deletePost($postId, $userId) {
        $stmt = $this->db->prepare("DELETE FROM Posts WHERE ID = ? AND UserId = ?");
        $stmt->bind_param("ii", $postId, $userId);
        $stmt->execute();
        $stmt->close();
    }

    function getPost($postId, $userId) {
        $arr = array();
        $stmt = $this->db->prepare("SELECT Alias, (SELECT COUNT(*) FROM Following WHERE Follower = ? AND Following = Posts.UserId), Posts.Category, (Posts.Image IS NOT NULL) AS HasPicture, Posts.ID, Posts.UserId, Posts.Post, (SELECT COUNT(*) FROM Stats WHERE PostId = Posts.ID AND Type = 'Like') AS Likes, (SELECT COUNT(*) FROM Stats WHERE PostId = Posts.ID AND UserId = ? AND Type = 'Like') AS Liked, (SELECT COUNT(*) FROM Stats WHERE PostId = Posts.ID AND Type = 'Dislike') AS Dislikes, (SELECT COUNT(*) FROM Stats WHERE PostId = Posts.ID AND UserId = ? AND Type = 'Dislike') AS Disliked FROM Posts INNER JOIN Users ON Posts.UserID = Users.ID WHERE Posts.ID = ?");
        $stmt->bind_param("iiii", $userId, $userId, $userId, $postId);
        $stmt->execute();
        $stmt->bind_result($alias, $following, $cat, $hasPic, $id, $userId, $post, $likes, $liked, $dislikes, $disliked);

        while ($stmt->fetch()) {
            array_push($arr, array("UserId" => $userId, "Category" => $cat, "HasImage" => $hasPic, "Image" => "http://www.randommash.com/Templates/Image.php?id=" . $id, "PostId" => $id, "Post" => $post, "Likes" => $likes, "Liked" => $liked, "Dislikes" => $dislikes, "Disliked" => $disliked, "Alias" => $alias, "Following" => ($following == 0) ? false : true));
        }

        echo json_encode($arr);

        $stmt->close();
    }

    function getPosts($timeframe, $category, $userId, $user = 0) {
        $arr = array();
        $initQuery = "SELECT Alias, (SELECT COUNT(*) FROM Following WHERE Follower = ? AND Following = Posts.UserId), Posts.Category, Posts.ID, Posts.UserId, Posts.Post, (Posts.Image IS NOT NULL) AS HasPicture, (SELECT COUNT(*) FROM Stats WHERE PostId = Posts.ID AND Type = 'Like') AS Likes, (SELECT COUNT(*) FROM Stats WHERE PostId = Posts.ID AND UserId = ? AND Type = 'Like') AS Liked, (SELECT COUNT(*) FROM Stats WHERE PostId = Posts.ID AND Type = 'Dislike') AS Dislikes, (SELECT COUNT(*) FROM Stats WHERE PostId = Posts.ID AND UserId = ? AND Type = 'Dislike') AS Disliked FROM Posts INNER JOIN Users ON Posts.UserID = Users.ID ";
        if ($category != "All") {
            $initQuery .= "WHERE Category = '" . $category . "' ";
        }

        switch ($timeframe) {
            case "user": {
                    $query = "Posts.UserId = '" . $user . "' ";
                    $initQuery .= (($category == "All") ? "WHERE " . $query : "AND " . $query) . " ORDER BY PostedWhen DESC ";

                    break;
                }
            case "following": {
                    $query = "(Posts.UserId IN (SELECT Following FROM Following WHERE Follower = '" . $userId . "') OR Posts.UserId = '" . $userId . "') ORDER BY PostedWhen DESC ";

                    $initQuery .= ($category == "All") ? "WHERE " . $query : "AND " . $query;

                    break;
                }
            case "top": {
                    $initQuery .= "ORDER BY Likes DESC, PostedWhen DESC ";

                    break;
                }
            case "recent": {
                    $initQuery .= "ORDER BY PostedWhen DESC ";

                    break;
                }
            case "worst": {
                    $initQuery .= "ORDER BY Dislikes DESC, Likes ASC, PostedWhen DESC ";

                    break;
                }
        }

        $initQuery .= "LIMIT 25";
        $stmt = $this->db->prepare($initQuery);

        $stmt->bind_param("iii", $userId, $userId, $userId);
        $stmt->execute();
        $stmt->bind_result($alias, $following, $cat, $id, $userId, $post, $hasImage, $likes, $liked, $dislikes, $disliked);

        while ($stmt->fetch()) {
            array_push($arr, array("UserId" => $userId, "PostId" => $id, "Post" => $post, "Category" => $cat, "Likes" => $likes, "Liked" => $liked, "Dislikes" => $dislikes, "Disliked" => $disliked, "Alias" => $alias, "HasImage" => $hasImage, "Image" => "http://www.randommash.com/Templates/Image.php?id=" . $id, "Following" => ($following == 0) ? false : true));
        }

        echo json_encode($arr);
        $stmt->close();
    }

    #Liked Dislike Handling

    function getLiked($postId, $UID = 0) {
        $arr = array();
        $stmt = $this->db->prepare("SELECT Alias, UserId, (SELECT COUNT(*) FROM Following WHERE Follower = ? AND Following = UserId) FROM Stats INNER JOIN Users ON Stats.UserId = Users.Id WHERE Type = 'Like' AND PostId = ?");
        $stmt->bind_param("ii", $UID, $postId);
        $stmt->execute();
        $stmt->bind_result($alias, $userId, $following);

        while ($stmt->fetch()) {
            array_push($arr, array("Alias" => $alias, "UserId" => $userId, "Following" => ($following == 0) ? false : true));
        }

        echo json_encode($arr);

        $stmt->close();
    }

    function getDisliked($postId, $UID = 0) {
        $arr = array();
        $stmt = $this->db->prepare("SELECT Alias, UserId, (SELECT COUNT(*) FROM Following WHERE Follower = ? AND Following = UserId) FROM Stats INNER JOIN Users ON Stats.UserId = Users.Id WHERE Type = 'Dislike' AND PostId = ?");
        $stmt->bind_param("ii", $UID, $postId);
        $stmt->execute();
        $stmt->bind_result($alias, $userId, $following);

        while ($stmt->fetch()) {
            array_push($arr, array("Alias" => $alias, "UserId" => $userId, "Following" => ($following == 0) ? false : true));
        }

        echo json_encode($arr);

        $stmt->close();
    }

    function disorlikePost($userId, $postId, $dis) {
        $statement = "";
        $stmt = $this->db->prepare("SELECT COUNT(*) FROM Stats WHERE PostId = ? AND Type = ? AND UserId = ?");
        $stmt->bind_param("isi", $postId, $dis, $userId);
        $stmt->execute();
        $stmt->bind_result($count);

        while ($stmt->fetch()) {
            break;
        }

        $stmt->close();

        if ($count == 0)
            $statement = "INSERT INTO Stats (PostId, UserId, Type) VALUES (?, ?, ?)";
        else
            $statement = "DELETE FROM Stats WHERE PostId = ? AND UserId = ? AND Type = ?";
        $stmt = $this->db->prepare($statement);
        $stmt->bind_param("iis", $postId, $userId, $dis);
        $stmt->execute();
        $stmt->close();

        $stmt = $this->db->prepare("SELECT UserId, (SELECT Alias FROM Users WHERE ID = ?) AS Alias FROM Posts WHERE ID = ?");
        $stmt->bind_param("ii", $userId, $postId);
        $stmt->execute();
        $stmt->bind_result($posterId, $alias);

        while ($stmt->fetch()) {
            break;
        }

        $stmt->close();

        if ($posterId != $userId && $count == 0) {
            $this->notifAPI->sendNotification($posterId, $alias . " " . strtolower($dis) . "d your post.", $postId, $userId);
            $this->notifAPI->SendNotifications(array($posterId), $alias . " " . strtolower($dis) . "d your post.", array("PostId" => $postId));
        }
    }

    #Comment Handling

    function getPostComments($postId) {
        $arr = array();
        $stmt = $this->db->prepare("SELECT Comments.ID, Comment, Comments.UserID, Alias FROM Comments INNER JOIN Users ON Comments.UserID = Users.ID WHERE Comments.PostId = ?");
        $stmt->bind_param("i", $postId);
        $stmt->execute();
        $stmt->bind_result($id, $comment, $userId, $alias);

        while ($stmt->fetch()) {
            array_push($arr, array("Comment" => $comment, "CommentID" => $id, "UID" => $userId, "Alias" => $alias));
        }

        echo json_encode($arr);

        $stmt->close();
    }

    function postComment($post, $postId, $userId) {
        $alias = $this->getAlias($userId, true);
        $stmt = $this->db->prepare("INSERT INTO Comments (Comment, PostId, UserId) VALUES (?, ?, ?)");
        $stmt->bind_param("sii", $post, $postId, $userId);
        $stmt->execute();

        while ($stmt->fetch()) {
            break;
        }

        $stmt->close();

        $arr = array();
        $stmt = $this->db->prepare("SELECT UserId FROM Posts WHERE ID = ?");
        $stmt->bind_param("i", $postId);
        $stmt->execute();
        $stmt->bind_result($posterId);

        while ($stmt->fetch()) {
            if ($userId != $posterId) {
                $this->notifAPI->sendNotification($posterId, $alias . " commented on your post.", $postId, $userId);
                $this->notifAPI->SendNotifications($posterId, $alias . " commented on your post.", array("PostId" => $postId));
            }
        }

        $stmt->close();

        $stmt = $this->db->prepare("SELECT DISTINCT UserID FROM Comments WHERE PostID = ? AND UserID != ?");
        $stmt->bind_param("ii", $postId, $userId);
        $stmt->execute();
        $stmt->bind_result($user);

        while ($stmt->fetch()) {
            if ($user != $posterId && $userId != $user)
                array_push($arr, $user);
        }

        $stmt->close();

        foreach ($arr as $str) {
            $this->notifAPI->sendNotification($str, $alias . " also commented on a post.", $postId, $userId);
        }

        if (count($arr) > 0)
            $this->notifAPI->SendNotifications($arr, $alias . " also commented on a post.", array("PostId" => $postId));
    }

    function deleteComment($commentId, $postId, $userId) {
        $stmt = $this->db->prepare("DELETE FROM Comments WHERE ID = ? AND PostId = ? AND UserId = ?");
        $stmt->bind_param("iii", $commentId, $postId, $userId);
        $stmt->execute();
        $stmt->close();
    }

}

class NotificationAPI {

    private $db;
    private $category;

    #MISC Handling

    function __construct() {
        $this->db = new mysqli('localhost', 'randomma', “password”, 'randomma_rm');
        $this->db->autocommit(TRUE);
    }

    function __destruct() {
        $this->db->close();
    }

    #Notification Handling

    function sendRandomPost($uid = 0) {
        $this->category = 'com.tests.posts';

        $users = array();

        if ($uid === 0) {
            $stmt = $this->db->prepare("SELECT ID FROM Users");
            $stmt->execute();
            $stmt->bind_result($uid);

            while ($stmt->fetch()) {
                array_push($users, $uid);
            }

            $stmt->close();
        }
        else array_push($users, $uid);

        foreach ($users as $user) {
            $arr = array();

            $stmt = $this->db->prepare("SELECT ID, Post FROM Posts WHERE (SELECT COUNT(*) FROM Stats WHERE UserId = ? AND PostId = Posts.ID) = 0 AND UserId != ? ORDER BY RAND() LIMIT 1");
            $stmt->bind_param('ii', $user, $user);
            $stmt->execute();
            $stmt->bind_result($id, $post);

            while ($stmt->fetch()) {
                array_push($arr, array("PostId" => $id, "Post" => $post));
            }

            $stmt->close();
            $this->SendNotifications(array($user), $arr[0]["Post"], $arr[0]);
        }
    }

    function getMessages($userId) {
        $arr = array();
        $notif = array();

        $stmt = $this->db->prepare("SELECT ID, Data, Notifications.Read FROM Notifications WHERE UID = ? ORDER BY SentWhen DESC");
        $stmt->bind_param("i", $userId);
        $stmt->execute();
        $stmt->bind_result($id, $msg, $read);

        while ($stmt->fetch()) {
            array_push($notif, array("NotificationID" => $id, "Message" => $msg, "Read" => $read));
        }

        $stmt->close();

        $stmt = $this->db->prepare("SELECT Messages.ID, UserId, UserTo, Message, Messages.Read, Alias FROM Messages INNER JOIN Users ON Messages.UserId = Users.ID WHERE (Messages.UserTo = ? OR Messages.UserId = ?) ORDER BY SentWhen DESC");
        $stmt->bind_param("ii", $userId, $userId);
        $stmt->execute();
        $stmt->bind_result($id, $userId, $userTo, $msg, $read, $userAlias);

        while ($stmt->fetch()) {
            array_push($arr, array("MessageID" => $id, "UserFrom" => $userId, "UserFromAlias" => $userAlias, "UserTo" => $userTo, "Message" => $msg, "Read" => $read));
        }

        $stmt->close();
        echo json_encode(array($notif, $arr));
    }

    function getNotifications($userId) {
        $arr = array();
        $stmt = $this->db->prepare("SELECT ID, Data, Notifications.Read FROM Notifications WHERE UID = ? ORDER BY SentWhen DESC");
        $stmt->bind_param("i", $userId);
        $stmt->execute();
        $stmt->bind_result($id, $msg, $read);

        while ($stmt->fetch()) {
            array_push($arr, array("NotificationID" => $id, "Message" => json_decode($msg), "Read" => $read));
        }

        $stmt->close();
        echo json_encode($arr);
    }

    function readNotifications($msgId, $userId) {
        $stmt = $this->db->prepare("UPDATE Messages SET Messages.Read = 1 WHERE ID = ? AND UserTo = ?");
        $stmt->bind_param("ii", $msgId, $userId);
        $stmt->execute();
        $stmt->close();

        $stmt = $this->db->prepare("UPDATE Notifications SET Notifications.Read = 1 WHERE ID = ? AND UID = ?");
        $stmt->bind_param("ii", $msgId, $userId);
        $stmt->execute();
        $stmt->close();
    }

    function deleteNotifications($msgId, $userId) {
        $stmt = $this->db->prepare("DELETE FROM Messages WHERE ID = ? AND (UserTo = ? OR UserId = ?)");
        $stmt->bind_param("iii", $msgId, $userId, $userId);
        $stmt->execute();
        $stmt->close();

        $stmt = $this->db->prepare("DELETE FROM Notifications WHERE ID = ? AND UID = ?");
        $stmt->bind_param("ii", $msgId, $userId);
        $stmt->execute();
        $stmt->close();
    }

    function sendNotification($userId, $msg, $postId, $user) {
        $str = json_encode(array("Message" => $msg, "PostId" => $postId, "UserId" => $user));

        $stmt = $this->db->prepare("SELECT COUNT(*) FROM Notifications WHERE Data = ? AND UID = ?");
        $stmt->bind_param("si", $str, $userId);
        $stmt->execute();
        $stmt->bind_result($count);

        while ($stmt->fetch()) {
            break;
        }

        $stmt->close();

        if ($count == 0) {
            $stmt = $this->db->prepare("INSERT INTO Notifications (Data, UID) VALUES (?, ?)");
            $stmt->bind_param("si", $str, $userId);
            $stmt->execute();
            $stmt->close();
        }
    }

    function postMessage($post, $UID, $userId) {
        $stmt = $this->db->prepare("INSERT INTO Messages (UserId, UserTo, Message) VALUES (?, ?, ?)");
        $stmt->bind_param("iis", $UID, $userId, $post);
        $stmt->execute();
        $stmt->close();

        $stmt = $this->db->prepare("SELECT Alias FROM Users WHERE ID = ?");
        $stmt->bind_param("i", $UID);
        $stmt->execute();
        $stmt->bind_result($alias);

        while ($stmt->fetch()) {
            break;
        }

        $stmt->close();
        $this->SendNotifications(array($userId), $alias . " sent you a message.");
    }

    function getMessagesCount($userId) {
        $arr = array();
        $stmt = $this->db->prepare("SELECT (COUNT(*) + (SELECT COUNT(*) FROM Notifications WHERE UID = ? AND Notifications.Read = FALSE)) FROM Messages WHERE UserTo = ? AND Messages.Read = FALSE");
        $stmt->bind_param("ii", $userId, $userId);
        $stmt->execute();
        $stmt->bind_result($id);

        while ($stmt->fetch()) {
            array_push($arr, array("MessageCount" => $id));
        }

        $stmt->close();
        echo json_encode($arr);
    }

    function getNotificationCount($userId) {
        $stmt = $this->db->prepare("SELECT COUNT(*) FROM Notifications WHERE UID = ? AND Notifications.Read = FALSE");
        $stmt->bind_param("i", $userId);
        $stmt->execute();
        $stmt->bind_result($id);

        while ($stmt->fetch()) {
            echo $id;
        }

        $stmt->close();
    }

    #Device Handling

    function SendNotifications($UIDs, $message, $data = array()) {
        $query = "SELECT UUID, IsBeta, Devices.System, ((SELECT COUNT(*) FROM Messages WHERE UserTo = Devices.UID AND Messages.Read = 0) + (SELECT COUNT(*) FROM Notifications WHERE Notifications.UID = Devices.UID AND Notifications.Read = 0)) AS Badge FROM Devices WHERE UID IN (";
        foreach ($UIDs as $str) {
            $query .= $str . ",";
        }
        $query = substr($query, 0, -1);
        $query .= ")";

        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $stmt->bind_result($uuid, $isbeta, $system, $badge);

        while ($stmt->fetch()) {
            if ($system == "iOS")
                $this->sendPushNotification($message, $uuid, $badge, $data, $isbeta);
            else
                continue;
        }
    }

    function AddiOSDevice($UUID, $UID, $Beta = false) {
        error_log($UUID);
        $stmt = $this->db->prepare("DELETE FROM Devices WHERE UUID = ?");
        $stmt->bind_param("s", $UUID);
        $stmt->execute();
        $stmt->close();

        $stmt = $this->db->prepare("INSERT INTO Devices (UID, UUID, System, IsBeta) VALUES (?, ?, 'iOS', ?)");
        $stmt->bind_param("isi", $UID, $UUID, $Beta);
        $stmt->execute();
        $stmt->close();
    }

    #Android Push Notifications

    function sendGCMNotification($msg, $tokens) {
        $url = 'https://android.googleapis.com/gcm/send';
        $fields = array(
            'registration_ids' => $tokens,
            'data' => $msg,
        );
        // Google Cloud Messaging GCM API Key
        define("GOOGLE_API_KEY", "AIzaSyBIk2CuQlGuFucymbKpWLZNdv_9_S4FQbU");
        $headers = array(
            'Authorization: key=' . GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);

        echo $result;
    }

    #IOS Push notifications

    function sendPushNotification($msg, $token, $badge, $data, $isBeta = false) {
        $crt = "ssl://gateway.push.apple.com:2195";
        $pem = "Web Service/ckProd.pem";
        if ($isBeta) {
            $pem = "Web Service/ckDev.pem";
            $crt = "ssl://gateway.sandbox.push.apple.com:2195";
        } else {
            return;
        }

        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $pem);
        stream_context_set_option($ctx, 'ssl', 'passphrase', "i'llbeokay14");

        // Open a connection to the APNS server
        $fp = stream_socket_client($crt, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

        if (!$fp) {
            error_log($err);
            exit("Failed to connect:" . $err . $errstr . PHP_EOL);
        }

        // Create the payload body
        $body['aps'] = array(
            'alert' => $msg,
            'sound' => 'default',
            'badge' => $badge,
//            'action-loc-key' => "posts",
            'category' => $this->category
        );

        $body['data'] = $data;

        // Encode the payload as JSON
        $payload = json_encode($body);
//        echo $payload;

        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $token) . pack('n', strlen($payload)) . $payload;

        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));
        fclose($fp);
    }

}

?>