<?php
$title = "Welcome to Random Mash!";
include("Templates/Head.php");
?>
<div id="body">
    <?php
    if (isset($_POST["catDrop"]))
        $cat = $_POST["catDrop"];
    else
        $cat = "All";
    ?>

    <table style="width:50%; margin: auto">
        <tr style="vertical-align: bottom">
            <td colspan="3" style="border-bottom: 1px solid;">
                <table>
                    <tr style="vertical-align: bottom">
                        <td>
                            <a href="?t=top">Top</a> | <a href="?t=recent">Recent</a> <?php if ($api2->IsSignedIn()) echo "| <a href='?t=following'>Following</a>" ?>
                        </td>
                        <td style="text-align: right">
                            <form style="margin: auto; text-align: right; width: 100%" action="" method="post">
                                Categories:
                                <select name="catDrop" onChange="this.form.submit();">
                                    <?php
                                    echo $api2->categoriesNew();
                                    ?>
                                </select>
                            </form>
                            <script type="text/javascript">
                                document.getElementsByName("catDrop")[0].value = '<?php echo $cat ?>';
                            </script>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="padding: 10px">
                <?php
                include("Templates/Post.php");
                ?>
            </td>
        </tr>
        <?php
        if (isset($_GET["i"]))
        {
            $api2->postId = $_GET["i"];
            $api2->getPosts(null, "All");
        }
        else if (isset($_GET["t"])) {
            switch ($_GET["t"]) {
                case "top": {
                        $api2->getPosts("top", $cat);

                        break;
                    }
                case "recent": {
                        $api2->getPosts("recent", $cat);

                        break;
                    }
                case "following": {
                        $api2->getPosts("following", $cat);
                        break;
                    }
                default:
                    break;
            }
        }
        else
            $api2->getPosts("top", $cat);
        ?>
    </table>
</div>